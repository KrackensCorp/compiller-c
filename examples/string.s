.LC3:
	.string "TRUE\n"
.LC2:
	.string "FALSE\n"
.LC1:
	.string " содержит строку '%s'?\n"
.LC0:
	.string "Строка  '%s'"
.global main
	.text
	.type main, @function
main:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$2048, %rsp
	movabsq	$0x61616361, %rax
	movq	%rax, -12(%rbp)
	movabsq	$0x616163, %rax
	movq	%rax, -24(%rbp)
	leaq	-12(%rbp),	%rax
	movq	%rax,	%rsi
	movl	$.LC0,	%edi
	call	printf
	leaq	-24(%rbp),	%rax
	movq	%rax,	%rsi
	movl	$.LC1,	%edi
	call	printf
	leaq	-24(%rbp),	%rdx
	leaq	-12(%rbp),	%rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strstr
	movq	%rax, -36(%rbp)
	cmp	$0,	-36(%rbp)
	jne	.if11
	movl	$.LC2,	%edi
	call	printf
	jmp	.if12
.if11:
	movl	$.LC3,	%edi
	call	printf
.if12:
	leave
	ret
