.LC1:
	.string "\n Максимальный элемент %d\n"
.LC0:
	.string "\n Минимальный элемент %d\n"
.global main
	.text
	.type main, @function
main:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$2048, %rsp
	movl	$0, -4(%rbp)
	movl	$10, -8(%rbp)
	movl    $13 , -16(%rbp)
	movl    $7 , -20(%rbp)
	movl    $3 , -24(%rbp)
	movl    $4 , -28(%rbp)
	movl    $5 , -32(%rbp)
	movl    $6 , -36(%rbp)
	movl    $22 , -40(%rbp)
	movl    $8 , -44(%rbp)
	movl    $1 , -48(%rbp)
	movl    $10 , -52(%rbp)
	movl	-16(%rbp), %eax
	movl	%eax, -56(%rbp)
	movl	-16(%rbp), %eax
	movl	%eax, -60(%rbp)
	jmp	.while10
.while11:
	movl    -4(%rbp), %eax
	cltd
	movl    -52(%rbp,%rax,4), %eax
	cmpl	-56(%rbp),	%eax
	jge	.if21
	movl	-4(%rbp), %eax
	cltd
	movl	-52(%rbp,%rax,4), %eax
	movl	%eax, -56(%rbp)
.if21:
	movl	-4(%rbp), %edx
	addl	$1, %edx
	movl	%edx, -4(%rbp)
.while10:
	movl	-4(%rbp),	%eax
	cmpl	-8(%rbp),	%eax
	jl	.while11
	movl	$0, -4(%rbp)
	jmp	.while30
.while31:
	movl    -4(%rbp), %eax
	cltd
	movl    -52(%rbp,%rax,4), %eax
	cmpl	-60(%rbp),	%eax
	jle	.if41
	movl	-4(%rbp), %eax
	cltd
	movl	-52(%rbp,%rax,4), %eax
	movl	%eax, -60(%rbp)
.if41:
	movl	-4(%rbp), %edx
	addl	$1, %edx
	movl	%edx, -4(%rbp)
.while30:
	movl	-4(%rbp),	%eax
	cmpl	-8(%rbp),	%eax
	jl	.while31
	movl	-56(%rbp),	%eax
	movl	%eax,	%esi
	movl	$.LC0,	%edi
	call	printf
	movl	-60(%rbp),	%eax
	movl	%eax,	%esi
	movl	$.LC1,	%edi
	call	printf
	leave
	ret
