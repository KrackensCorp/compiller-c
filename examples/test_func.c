int square(int a){
    a = a * a;
    return a;
}

int sum(int i, int j){
    int sum =  i + j;
    return sum;
}

int main(){
    int a = 5;
    printf("Число a = %d\n", a);
    a = square(a);
    printf("Его квадрат %d\n", a);
    int b = sum(a, 10);
    printf("Сумма квадрата a и 10 = %d + 10 = %d\n",a, b);
}