.LC2:
	.string "Сумма квадрат a и 10 = %d + 10 = %d\n"
.LC1:
	.string "Его квадрат %d\n"
.LC0:
	.string "Число a = %d\n"
square:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$2048, %rsp
	movl	%edi,	-4(%rbp)
	movl	-4(%rbp), %edx
	movl	-4(%rbp), %eax
	mull	%edx
	movl	%eax, -4(%rbp)
	movl	-4(%rbp),	%eax
	leave
	ret
sum:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$2048, %rsp
	movl	%edi,	-8(%rbp)
	movl	%esi,	-12(%rbp)
	movl	-8(%rbp), %edx
	addl	-12(%rbp), %edx
	movl	%edx, -16(%rbp)
	movl	-16(%rbp),	%eax
	leave
	ret
.global main
	.text
	.type main, @function
main:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$2048, %rsp
	movl	$5, -20(%rbp)
	movl	-20(%rbp),	%eax
	movl	%eax,	%esi
	movl	$.LC0,	%edi
	call	printf
	movl	-20(%rbp), %eax
	movl	%eax, %edi
	call	square
	movl	%eax,	-20(%rbp)
	movl	-20(%rbp),	%eax
	movl	%eax,	%esi
	movl	$.LC1,	%edi
	call	printf
	movl	-20(%rbp), %eax
	movl	%eax, %edi
	movl	$10, %esi
	call	sum
	movl	%eax,	-24(%rbp)
	movl	-24(%rbp),	%edx
	movl	-20(%rbp),	%eax
	movl	%eax,	%esi
	movl	$.LC2,	%edi
	call	printf
	leave
	ret
