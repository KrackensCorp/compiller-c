.LC9:
	.string "%d\n"
.LC8:
	.string "Вычитание b из a\n"
.LC7:
	.string "%d\n"
.LC6:
	.string "Сложение а с b\n"
.LC5:
	.string "%d\n"
.LC4:
	.string "Умножение а на b\n"
.LC3:
	.string "%d\n"
.LC2:
	.string "Деление а на b\n"
.LC1:
	.string "Переменная а = %d\nПеременная b = %d\n"
.LC0:
	.string "Тест математических функций\n"
.global main
	.text
	.type main, @function
main:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$2048, %rsp
	movl	$49, -4(%rbp)
	movl	$4, -8(%rbp)
	movl	$.LC0,	%edi
	call	printf
	movl	-8(%rbp),	%edx
	movl	-4(%rbp),	%eax
	movl	%eax,	%esi
	movl	$.LC1,	%edi
	call	printf
	movl	$.LC2,	%edi
	call	printf
	movl	-4(%rbp), %eax
	cltd
	cltd
	idivl	-8(%rbp)
	movl	%eax, -12(%rbp)
	movl	-12(%rbp),	%eax
	movl	%eax,	%esi
	movl	$.LC3,	%edi
	call	printf
	movl	$.LC4,	%edi
	call	printf
	movl	-4(%rbp), %edx
	movl	-8(%rbp), %eax
	mull	%edx
	movl	%eax, -12(%rbp)
	movl	-12(%rbp),	%eax
	movl	%eax,	%esi
	movl	$.LC5,	%edi
	call	printf
	movl	$.LC6,	%edi
	call	printf
	movl	-4(%rbp), %edx
	addl	-8(%rbp), %edx
	movl	%edx, -12(%rbp)
	movl	-12(%rbp),	%eax
	movl	%eax,	%esi
	movl	$.LC7,	%edi
	call	printf
	movl	$.LC8,	%edi
	call	printf
	movl	-4(%rbp), %edx
	subl	-8(%rbp), %edx
	movl	%edx, -12(%rbp)
	movl	-12(%rbp),	%eax
	movl	%eax,	%esi
	movl	$.LC9,	%edi
	call	printf
	leave
	ret
