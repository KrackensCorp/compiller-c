package idTable

import lexer.TokenType

class Variable constructor(value: String, tokenType: TokenType?) {
    private var value: String? = null
    private var tokenType: TokenType? = null

    init {
        this.value = value
        this.tokenType = tokenType
    }

    override fun toString(): String {
        return "Variable{" +
                "value='" + value + "'" +
                ", tokenType=" + tokenType +
                '}'
    }

    fun getValue(): String? {
        return value
    }

    fun setValue(value: String) {
        this.value = value
    }

    fun getTokenType(): TokenType? {
        return tokenType
    }

    fun setTokenType(tokenType: TokenType) {
        this.tokenType = tokenType
    }
}