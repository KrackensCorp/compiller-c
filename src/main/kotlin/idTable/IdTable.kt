package idTable

import lexer.TokenType
import lexer.TokenType.*
import parser.Node
import java.util.*
import kotlin.system.exitProcess

class IdTable(tree: Node) {

    private var isAnnounced = false
    private var idTable: MutableMap<String, MutableList<Variable>>? = null
    private var subLevel: MutableMap<Int, Char>? = null
    private var level: Int = 0
    private var type: TokenType? = null
    private var tree: Node? = null

    init {
        this.idTable = HashMap()
        this.subLevel = HashMap()
        this.level = 0
        this.tree = tree
        formATablel()
    }

    fun getIdTable(): MutableMap<String, MutableList<Variable>>? {
        return this.idTable
    }

    private fun getLevel(): Int {
        return level
    }

    private fun setLevel(level: Int) {
        this.level = level
    }

    private fun addSubLevel(level: Int) {
        var subLvl: Char? = subLevel?.get(level)
        if (subLvl == null) {
            subLevel?.put(level, 'a')
        } else {
            if (level == 0) {
                subLevel?.put(level, 'a')
            } else {
                subLvl = (subLvl.toChar() + 1)
                subLevel?.put(level, subLvl)
            }
        }
    }

    private fun remSubLevel(level: Int) {
        var subLvl: Char? = subLevel?.get(level)
        subLvl = (subLvl!!.toInt() - 1).toChar()
        subLevel!![level] = subLvl
    }

    private fun tableParams(child: Node) {
        setLevel(getLevel() + 1)
        addSubLevel(level)
        for (childParam in child.getListChild()) {
            when (childParam.getTokenType()) {
                PARAM -> {
                    val lvl = getLevel().toString() + subLevel?.get(getLevel()).toString()

                    var testList: MutableList<Variable>? = idTable?.get(childParam.getTokenValue().toString())
                    if (testList == null) {
                        testList = ArrayList()
                    }
                    testList.add(Variable(lvl, childParam.getFirstChildren().getFirstChildren().getTokenType())) // поменять тип
                    idTable?.put(childParam.getTokenValue().toString(), testList)
                }
                else -> {
                }
            }
        }
        remSubLevel(level)
        setLevel(getLevel() - 1)
    }

    private fun body(child: Node) {
        addSubLevel(level)
        for (childFunc in child.getListChild()) {
            when (childFunc.getTokenType()) {
                BODY_THEN, BODY_ELSE, BODY -> adding(childFunc)
                NAME -> checkName(childFunc)
                TYPE -> {
                    type = childFunc.getFirstChildren().getTokenType()
                    isAnnounced = true
                }
                PARAMS_LIST -> tableParams(childFunc)
                else -> {
                }
            }
        }
    }

    private fun checkName(childFunc: Node) {
        if (isAnnounced) {
            val lvl = getLevel().toString() + subLevel?.get(getLevel()).toString()

            var testList: MutableList<Variable>? = idTable?.get(childFunc.getTokenValue().toString())
            if (testList == null) {
                testList = ArrayList()
            } else {
                for (check: Variable in testList) {
                    if (check.getValue() == lvl) {
                        println("ID: повторное объявление переменной на одном уровне LOC<" + childFunc.getValue()?.row
                                + ":" + childFunc.getValue()?.col + ">")
                        exitProcess(0)
                    }
                }
            }
            testList.add(Variable(lvl, type))
            idTable?.put(childFunc.getTokenValue().toString(), testList)
            isAnnounced = false
        }
    }

    private fun adding(childFunc: Node) {
        setLevel(getLevel() + 1)
        addSubLevel(level)
        for (childBody in childFunc.getListChild()) {
            if (childBody.getTokenType() === COMMAND) {
                for (childCommand in childBody.getListChild()) {
                    when (childCommand.getTokenType()) {
                        BODY_THEN, BODY_ELSE, BODY -> adding(childCommand)
                        NAME -> checkName(childCommand)
                        TYPE -> {
                            type = childCommand.getFirstChildren().getTokenType()
                            isAnnounced = true
                        }
                        else -> {
                        }
                    }
                }
            } else if (childBody.getTokenType() === EMPTY) {
                setLevel(getLevel() - 1)
            }
        }
    }

    private fun formATablel() {
        val listChild: MutableList<Node>
        if (tree != null) {
            listChild = tree!!.getListChild()
            for (child: Node in listChild) {
                if (child.getTokenType() === FUNCTION) {
                    if (child.getListChild().size > 0) {
                        body(child)
                    }
                }
            }
        }
    }
}
