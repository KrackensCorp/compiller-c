package buffer

import java.io.Reader
import kotlin.system.exitProcess

const val DEFAULT_SIZE_BUFFER: Int = 10
const val END_OF_SOURCE_CODE: Int = -1

class Buffer(reader: Reader, capacity: Int = DEFAULT_SIZE_BUFFER) {

    private var reader: Reader
    private var maxIndex = 0
    private var localBuf: CharArray
    private var indexBuf = 0
    private var isEndSourceCode = false
    private var row = 1
    private var col = 0

    init {
        require(capacity >= 2) { "B: емкость должна быть больше 1" }
        this.reader = reader
        localBuf = CharArray(capacity)
        writeInBuffer()
    }

    fun getChar(): Char {
        if (isBufferEnded(0)) {
            writeInBuffer()
        }
        col++
        if (localBuf[indexBuf] == '\n') {
            col = 0
            row++
        }
        return localBuf[indexBuf++]
    }

    fun peekChar(): Int {
        if (isBufferEnded(0)) {
            writeInBuffer()
        }
        if (isEndSourceCode) {
            return END_OF_SOURCE_CODE
        }
        return localBuf[indexBuf].toInt()
    }

    fun peekSecondChar(): Int {
        if (isBufferEnded(1)) {
            writeInBuffer()
        }
        return if (isEndSourceCode) {
            END_OF_SOURCE_CODE
        } else localBuf[indexBuf + 1].toInt()
    }

    private fun isBufferEnded(shift: Int): Boolean = (indexBuf + shift) == maxIndex

    private fun writeInBuffer() {
        if (isBufferEnded(1) && !isBufferEnded(0)) {
            localBuf[0] = localBuf[indexBuf]
            indexBuf = 1
        } else {
            indexBuf = 0
        }
        try {
            val numCharRead = reader.read(localBuf, indexBuf, localBuf.size - indexBuf)
            if (numCharRead == END_OF_SOURCE_CODE) {
                isEndSourceCode = true
            } else {
                maxIndex = numCharRead + indexBuf
            }
        } catch (e: Exception) {
            println("B: ошибка чтения буфера\n")
            exitProcess(0)
        }
    }

    fun getRow(): Int {
        return row
    }

    fun getCol(): Int {
        return col
    }
}
