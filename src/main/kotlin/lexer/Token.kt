package lexer

class Token<T>(type: TokenType) {

    private var type: TokenType
    private var value: T?

    var row = 0
    var col = 0

    init {
        this.type = type
        this.value = null
    }

    constructor(type: TokenType, tokenValue: T) : this(type) {
        this.type = type
        this.value = tokenValue
    }

    fun getTokenValue(): T? {
        return value
    }

    fun getTokenType(): TokenType {
        return type
    }

    constructor(type: TokenType, row: Int, col: Int) : this(type) {
        this.value = null
        this.row = row
        this.col = col
    }

    constructor(type: TokenType, value: T, row: Int, col: Int) : this(type) {
        this.value = value
        this.row = row
        this.col = col
    }

    fun match(type: TokenType): Boolean {
        return this.type == type
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other == null) {
            return false
        }
        if (other !is Token<*>) {
            return false
        }
        if (type != other.type) {
            return false
        }
        return if (value == null) {
            other.value == null
        } else return value == other.value
    }

    override fun toString() = "Token{type=${this.type}, value=${this.value}, row=${this.row}, col=${this.col}}"

    override fun hashCode(): Int {
        var result = type.hashCode()
        result = 31 * result + (value?.hashCode() ?: 0)
        result = 31 * result + row
        result = 31 * result + col
        return result
    }
}
