package lexer

import buffer.Buffer
import kotlin.math.pow


class Lexer(private var buffer: Buffer) {
    private var currentToken: Token<*>? = null
    private var parentToken: Token<*>? = null
    private var isEndSourceCode = false

    fun getParentToken(): Token<*>? {
        return parentToken
    }

    fun peekToken(): Token<*>? {
        if (currentToken == null) {
            makeToken()
        }
        return currentToken
    }

    fun getToken(): Token<*>? {
        if (currentToken == null) {
            makeToken()
        }
        parentToken = currentToken
        val result: Token<*>? = currentToken
        makeToken()
        return result
    }

    private fun makeToken() {
        if (isEndSourceCode) {
            currentToken = Token(TokenType.END, "End", buffer.getRow(), buffer.getRow())
            return
        }

        readThroughSpacesAndComments()

        if (isEndSourceCode) {
            this.currentToken = Token(TokenType.END, "End", buffer.getRow(), buffer.getRow())
            return
        }

        when (val curChar: Char = buffer.getChar()) {
            '+' -> currentToken = Token<Any>(TokenType.PLUS, buffer.getRow(), buffer.getCol())
            '-' -> currentToken = Token<Any>(TokenType.MINUS, buffer.getRow(), buffer.getCol())
            '*' -> currentToken = Token<Any>(TokenType.MULTIPLICATION, buffer.getRow(), buffer.getCol())
            '/' -> currentToken = Token<Any>(TokenType.DIVISION, buffer.getRow(), buffer.getCol())
            '^' -> currentToken = Token<Any>(TokenType.EXPONENTIATION, buffer.getRow(), buffer.getCol())
            '(' -> currentToken = Token<Any>(TokenType.BRACKET_OPEN, buffer.getRow(), buffer.getCol())
            ')' -> currentToken = Token<Any>(TokenType.BRACKET_CLOSE, buffer.getRow(), buffer.getCol())
            '{' -> currentToken = Token<Any>(TokenType.BRACE_OPEN, buffer.getRow(), buffer.getCol())
            '}' -> currentToken = Token<Any>(TokenType.BRACE_CLOSE, buffer.getRow(), buffer.getCol())
            '[' -> currentToken = Token<Any>(TokenType.BRACET_OPEN, buffer.getRow(), buffer.getCol())
            ']' -> currentToken = Token<Any>(TokenType.BRACET_CLOSE, buffer.getRow(), buffer.getCol())
            ',' -> currentToken = Token<Any>(TokenType.COMMA, buffer.getRow(), buffer.getCol())
            ';' -> currentToken = Token<Any>(TokenType.SEMICOLON, buffer.getRow(), buffer.getCol())
            else -> {
                currentToken = if (Character.isDigit(curChar)) {
                    getNumberFromBuffer(curChar)
                } else {
                    if (Character.isAlphabetic(curChar.toInt()) || curChar == '_') {
                        getIdentificatorFromBuffer(curChar)
                    } else {
                        getConditionSignFromBuffer(curChar)
                    }
                }
            }
        }
    }

    private fun readThroughSpacesAndComments() {
        while (true) {
            while (Character.isWhitespace(peekCharFromBuffer(0))) {
                buffer.getChar()
            }

            var nextChar: Int = peekCharFromBuffer(0)

            if (nextChar == '/'.toInt()) {

                nextChar = peekCharFromBuffer(1)

                when (nextChar) {
                    '/'.toInt() -> {
                        while (!isEndSourceCode) {
                            if ('\n'.toInt() != peekCharFromBuffer(0)) {
                                buffer.getChar()
                            } else {
                                buffer.getChar()
                            }
                        }
                    }
                    '*'.toInt() -> {
                        buffer.getChar()
                        buffer.getChar()
                        var isEndMultilineComment = false
                        while (!isEndMultilineComment) {
                            if (isEndSourceCode) {
                                return
                            } else {
                                val nextCommentChar = buffer.getChar()
                                if (nextCommentChar == '*') {
                                    if (peekCharFromBuffer(0) == '/'.toInt()) {
                                        if (!isEndSourceCode) {
                                            isEndMultilineComment = true
                                            buffer.getChar()
                                        } else {
                                            return
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else -> {
                        if (isEndSourceCode) {
                            isEndSourceCode = false
                        }
                        return
                    }
                }
            } else {
                return
            }
        }
    }

    private fun getNumberFromBuffer(curChar: Char): Token<*>? {
        var number = Character.getNumericValue(curChar)

        var shiftComma = -1

        while (Character.isDigit(peekCharFromBuffer(0)) || peekCharFromBuffer(0) == '.'.toInt()) {
            if (shiftComma > -1 || peekCharFromBuffer(0) == '.'.toInt()) {
                shiftComma++
            }
            if (shiftComma != 0) {
                number = 10 * number + Character.getNumericValue(buffer.getChar())
            } else {
                buffer.getChar()
            }
        }

        return when (shiftComma) {
            -1 -> Token(TokenType.NUMBER, number, buffer.getRow(), buffer.getCol())
            1 -> throw RuntimeException("L: число не может заканчиваться на точку")
            else -> {
                var doubleNumber = number.toDouble()
                val tenPower = 10.0.pow(shiftComma.toDouble())
                doubleNumber /= tenPower
                Token(TokenType.NUMBER, doubleNumber, buffer.getRow(), buffer.getCol())
            }
        }
    }

    private fun getIdentificatorFromBuffer(curChar: Char): Token<*>? {
        var ident = curChar.toString()

        while (Character.isAlphabetic(peekCharFromBuffer(0))) {
            ident += buffer.getChar()
        }

        val result: Token<*>?

        result = when (ident) {
            "return" -> Token<Any>(TokenType.RETURN, buffer.getRow(), buffer.getCol())
            "int" -> Token<String>(TokenType.INT, buffer.getRow(), buffer.getCol())
            "double" -> Token<String>(TokenType.DOUBLE, buffer.getRow(), buffer.getCol())
            "char" -> Token<String>(TokenType.CHAR, buffer.getRow(), buffer.getCol())
            "strstr" -> Token<Any>(TokenType.STRSTR, buffer.getRow(), buffer.getCol())
            "printf" -> Token<Any>(TokenType.PRINTF, buffer.getRow(), buffer.getCol())
            "scanf" -> Token<Any>(TokenType.SCANF, buffer.getRow(), buffer.getCol())
            "void" -> Token<Any>(TokenType.VOID, buffer.getRow(), buffer.getCol())
            "if" -> Token<Any>(TokenType.IF, buffer.getRow(), buffer.getCol())
            "else" -> Token<Any>(TokenType.ELSE, buffer.getRow(), buffer.getCol())
            "while" -> Token<Any>(TokenType.WHILE, buffer.getRow(), buffer.getCol())
            else -> Token(TokenType.NAME, ident, buffer.getRow(), buffer.getCol())
        }
        return result
    }

    private fun getConditionSignFromBuffer(curChar: Char): Token<*>? {
        var sign = curChar.toString()

        if (peekCharFromBuffer(0) == '='.toInt()) {
            sign += buffer.getChar()
        }

        val result: Token<*>?

        when (sign) {
            "=" -> result = Token<Any>(TokenType.ASSIGNMENT, buffer.getRow(), buffer.getCol())
            "<" -> result = Token(TokenType.SIGN, "<", buffer.getRow(), buffer.getCol())
            "<=" -> result = Token(TokenType.SIGN, "<=", buffer.getRow(), buffer.getCol())
            "==" -> result = Token(TokenType.SIGN, "==", buffer.getRow(), buffer.getCol())
            "!=" -> result = Token(TokenType.SIGN, "!=", buffer.getRow(), buffer.getCol())
            ">" -> result = Token(TokenType.SIGN, ">", buffer.getRow(), buffer.getCol())
            ">=" -> result = Token(TokenType.SIGN, ">=", buffer.getRow(), buffer.getCol())
            "\"" -> {
                sign = ""
                while (peekCharFromBuffer(0) != '"'.toInt()) {
                    sign += if (peekCharFromBuffer(0) != '\n'.toInt()) {
                        buffer.getChar()
                    } else {
                        throw RuntimeException("L: незакрытый литерал")
                    }
                }
                buffer.getChar()
                result = Token(TokenType.LITERAL, sign, buffer.getRow(), buffer.getCol())
            }
            "'" -> {
                sign = ""
                sign += buffer.getChar()
                if (peekCharFromBuffer(0) == '\''.toInt()) {
                    buffer.getChar()
                } else {
                    throw RuntimeException("L: ошибка символов")
                }
                result = Token(TokenType.CHAR, sign[0], buffer.getRow(), buffer.getCol())
            }
            else -> throw RuntimeException("L: неопределённая лексема")
        }
        return result
    }

    private fun peekCharFromBuffer(serialIndex: Int): Int {
        var ch: Int = buffer.peekChar()

        if (ch == -1) {
            isEndSourceCode = true
        } else {
            if (serialIndex == 1) {
                ch = buffer.peekSecondChar()
                if (ch == -1) {
                    isEndSourceCode = true
                }
            }
        }
        return ch
    }
}