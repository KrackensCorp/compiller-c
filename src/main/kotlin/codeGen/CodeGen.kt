package codeGen

import lexer.TokenType
import parser.Node
import java.util.*
import kotlin.collections.HashMap
import kotlin.system.exitProcess

class CodeGen(tree: Node) {

    private var numberLC: Int = -1 // Вычислено эмпирическим путем
    private var tree: Node
    private var addressVar: MutableMap<String, Int>
    private var arrays: MutableMap<String, MutableList<String>>
    private var assembler: MutableList<String>

    private var varBytes: Int
    private var node: Int
    private var bodyCounter: Int
    private var nameVariable: String?
    private var returned: Boolean
    private var nameFunc: String?

    init {
        this.nameFunc = null
        this.returned = false
        this.nameVariable = null
        this.bodyCounter = 0
        this.varBytes = 0
        this.node = 0
        this.tree = tree
        this.addressVar = HashMap()
        this.arrays = HashMap()
        this.assembler = ArrayList()
        generator()
    }

    private fun isNumeric(strNum: String?): Boolean {
        try {
            val d = strNum!!.toDouble()
        } catch (nfe: NumberFormatException) {
            return false
        } catch (nfe: NullPointerException) {
            return false
        }
        return true
    }

    private fun setVar(name: String, type: TokenType) {
        when (type) {
            TokenType.INT -> varBytes += 4
            TokenType.CHAR -> varBytes += 12
            else -> {
            }
        }
        addressVar[name] = varBytes
    }

    private fun getNameLC(): String {
        return "LC$numberLC"
    }

    private fun setNameLC() {
        numberLC++
    }

    private fun getNameIf(bodyCounter: Int, numberIF: Int): String {
        val vall = bodyCounter.toString() + numberIF
        return ".if$vall"
    }

    private fun getNameIfNext(bodyCounter: Int, numberIF: Int): String {
        val nextVal = bodyCounter.toString() + (numberIF + 1)
        return ".if$nextVal"
    }

    private fun getNameWhile(bodyCounter: Int, numberWhile: Int): String {
        val vall = bodyCounter.toString() + numberWhile
        return ".while$vall"
    }

    private fun getNameWhileNext(bodyCounter: Int, numberWhile: Int): String? {
        val nextVal = bodyCounter.toString() + (numberWhile + 1)
        return ".while$nextVal"
    }

    fun getAssembler(): MutableList<String> {
        return assembler
    }

    private fun generator() {
        for (child: Node in tree.getListChild()) {
            if (child.getTokenType() == TokenType.FUNCTION) {
                returned = false
                functionParam(child)
            }
        }
    }

    private fun functionParam(body: Node) {
        for (functionParam: Node in body.getListChild()) {
            when (functionParam.getTokenType()) {
                TokenType.INT, TokenType.VOID -> assemblerFunctionNmae(functionParam.getFirstChildren())
                TokenType.PARAMS_LIST -> setAssemblerFunctionParam(functionParam)
                TokenType.BODY -> {
                    bodyCounter++
                    functionBody(functionParam)
                }
                else -> {
                }
            }
        }
    }

    private fun setAssemblerFunctionParam(functionParam: Node) {

        val names: MutableList<String> = ArrayList()

        for (param in functionParam.getListChild()) {
            if (param.getTokenType() == TokenType.PARAM) {
                when (param.getFirstChildren().getFirstChildren().getTokenType()) {
                    TokenType.INT -> {
                        val name = param.getTokenValue().toString()
                        setVar(name, TokenType.INT)
                        names.add(name)
                    }
                    TokenType.CHAR -> {
                        println("Нельзя принимать char")
                        exitProcess(0)
                    }
                    else -> {
                    }
                }
            }
        }

        if (names.size > 0) {
            var i = 0
            while (i < names.size) {
                when (i) {
                    0 -> assembler.add("\tmovl\t%edi,\t-" + addressVar[names[i]] + "(%rbp)")
                    1 -> assembler.add("\tmovl\t%esi,\t-" + addressVar[names[i]] + "(%rbp)")
                    2 -> assembler.add("\tmovl\t%edx,\t-" + addressVar[names[i]] + "(%rbp)")
                    else -> {
                        println("Входных параметров функции должно быть не более 3")
                        exitProcess(0)
                    }
                }
                i++
            }
        }
    }

    private fun assemblerFunctionNmae(functionName: Node) {
        nameFunc = functionName.getTokenValue().toString()
        if (nameFunc == "main") {
            assembler.add(".global main")
            assembler.add("\t.text")
            assembler.add("\t.type main, @function")
        }
        assembler.add("$nameFunc:")
        assembler.add("\tpushq\t%rbp")
        assembler.add("\tmovq\t%rsp, %rbp")
        assembler.add("\tsubq\t$2048, %rsp")
    }

    private fun functionBody(functionBody: Node) {
        for (body in functionBody.getListChild()) {
            when (body.getTokenType()) {
                TokenType.COMMAND -> assembler.addAll(bodyCommand(body)!!)
                TokenType.EMPTY -> {
                    assembler.add("\tleave")
                    assembler.add("\tret")
                }
                else -> {
                }
            }
        }
    }

    private fun bodyRec(body: Node): MutableList<String> {
        bodyCounter++
        val asm: MutableList<String> = ArrayList()
        var acm: MutableList<String>?
        for (bodyRec in body.getListChild()) {
            when (bodyRec.getTokenType()) {
                TokenType.COMMAND -> {
                    acm = bodyCommand(bodyRec)
                    if (acm != null) {
                        asm.addAll(acm)
                    }
                }
                else -> {
                }
            }
        }
        return asm
    }

    private fun bodyCommand(bodyCommand: Node): MutableList<String>? {
        var assemblerThen: MutableList<String> = ArrayList()
        var assemblerElse: MutableList<String> = ArrayList()

        var literalCheck = false
        var announcementVar = false
        var whileCommand = false
        var ifCommand = false

        val randForCommand = bodyCounter
        var numberWhile = 0
        var numberIf = 0

        var commandAssembler: MutableList<String> = ArrayList()
        val literal: MutableList<String> = ArrayList()

        for (command in bodyCommand.getListChild()) {
            when (command.getTokenType()) {
                TokenType.STRSTR -> strstr(command, commandAssembler)
                TokenType.TYPE -> announcementVar = true
                TokenType.CHAR -> charBodyCommand(command, announcementVar)
                TokenType.INT -> intBodyCommand(command, announcementVar, commandAssembler)
                TokenType.ARRAY -> if (announcementVar) {
                    commandAssembler = createArray(command)
                }
                TokenType.ASSIGNMENT -> {
                    assigment(command, commandAssembler)
                    node = 0
                }
                TokenType.ARRAYASSIGMENT -> arrayAssigment(command, commandAssembler)
                TokenType.RETURN -> returned = true
                TokenType.PRINTF, TokenType.SCANF -> {
                    inOutBodyCommand(command.getTokenType(), commandAssembler)
                    literalCheck = true
                }
                TokenType.PRINTF_BODY -> printfBody(command, literal, commandAssembler)
                TokenType.SCANF_BODY -> scanfBody(command, literal, commandAssembler)
                TokenType.WHILE -> {
                    whileCommand = true
                    commandAssembler.add("\tjmp\t" + getNameWhile(randForCommand, numberWhile))
                }
                TokenType.IF -> ifCommand = true
                TokenType.CONDITION -> conditionBodyCommand(command, commandAssembler, randForCommand,
                        numberWhile, numberIf, whileCommand, ifCommand)
                TokenType.BODY_THEN -> {
                    numberIf++
                    assemblerThen = bodyRec(command)
                }
                TokenType.BODY_ELSE -> assemblerElse = bodyRec(command)
                TokenType.BODY -> if (whileCommand) {
                    numberWhile++
                    commandAssembler.add(1, getNameWhile(randForCommand, numberWhile) + ":")
                    assemblerThen = bodyRec(command)
                    commandAssembler.addAll(2, assemblerThen)
                    numberWhile++
                }
                TokenType.EMPTY -> {
                    if (literalCheck) {
                        assembler.addAll(0, literal)
                    }
                    if (ifCommand) {
                        if (assemblerElse.size == 0) {
                            commandAssembler.addAll(assemblerThen)
                            commandAssembler.add(getNameIf(randForCommand, numberIf) + ":")
                        } else {
                            commandAssembler.addAll(assemblerThen)
                            commandAssembler.add("\tjmp\t" + getNameIf(randForCommand, numberIf + 1))
                            commandAssembler.add(getNameIf(randForCommand, numberIf) + ":")
                            commandAssembler.addAll(assemblerElse)
                            commandAssembler.add(getNameIf(randForCommand, numberIf + 1) + ":")
                        }
                    }
                    return commandAssembler
                }
                else -> {
                }
            }
        }
        return null
    }

    private fun conditionBodyCommand(command: Node, commandAssembler: MutableList<String>,
                                     randForCommand: Int, numberWhile: Int,
                                     numberIf: Int, whileCommand: Boolean, ifCommand: Boolean) {
        if (whileCommand) {
            condition(command, commandAssembler, randForCommand, numberWhile, TokenType.WHILE)
        }
        if (ifCommand) {
            condition(command, commandAssembler, randForCommand, numberIf, TokenType.IF)
        }
    }

    private fun inOutBodyCommand(type: TokenType?, commandAssembler: MutableList<String>) {
        setNameLC()
        when (type) {
            TokenType.PRINTF -> commandAssembler.add("\tcall\tprintf")
            TokenType.SCANF -> commandAssembler.add("\tcall\tscanf")
            else -> {
            }
        }
    }

    private fun intBodyCommand(command: Node, announcementVar: Boolean, commandAssembler: MutableList<String>) {
        nameVariable = command.getFirstChildren().getTokenValue().toString()
        if (announcementVar) {
            setVar(nameVariable!!, TokenType.INT)
        } else if (returned) {
            if (isNumeric(nameVariable!!)) {
                commandAssembler.add("\tmovl\t$$nameVariable, %eax")
            } else {
                commandAssembler.add("\tmovl\t-" + addressVar[nameVariable!!] + "(%rbp),\t%eax")
            }
        }
    }

    private fun charBodyCommand(command: Node, announcementVar: Boolean) {
        nameVariable = command.getFirstChildren().getTokenValue().toString()
        if (announcementVar) {
            setVar(nameVariable!!, TokenType.CHAR)
        } else if (returned) {
            println("Возвращать можно только int")
            exitProcess(0)
        }
    }

    private fun strstr(str: Node, assembler: MutableList<String>) {
        val name1: String = str.getListChild()[0].getFirstChildren().getTokenValue().toString()
        val name2: String = str.getListChild()[1].getFirstChildren().getTokenValue().toString()
        assembler.add("\tleaq\t-" + addressVar[name2] + "(%rbp),\t%rdx")
        assembler.add("\tleaq\t-" + addressVar[name1] + "(%rbp),\t%rax")
        assembler.add("\tmovq\t%rdx, %rsi")
        assembler.add("\tmovq\t%rax, %rdi")
        assembler.add("\tcall\tstrstr")
        assembler.add("\tmovq\t%rax, -" + addressVar[nameVariable] + "(%rbp)")
    }


    private fun condition(cond: Node?, assembler: MutableList<String>, randForCommand: Int, numberIfWhile: Int, type: TokenType) {
        var signType: String? = null

        var val1: String? = null
        var val2: String? = null

        var lit1: String? = null
        var lit2: String? = null

        var nameArray1: String? = null
        var nameArray2: String? = null

        var literal = false

        var count = 0

        for (condition in cond!!.getListChild()) {
            when (condition.getTokenType()) {
                TokenType.LITERAL -> {
                    literal = true
                    if (count == 0) {
                        lit1 = condition.getTokenValue().toString()
                        count++
                    } else {
                        lit2 = condition.getTokenValue().toString()
                    }
                }
                TokenType.NAME -> if (count == 0) {
                    nameArray1 = condition.getTokenValue().toString()
                    val1 = condition.getFirstChildren().getFirstChildren().getFirstChildren().getTokenValue().toString()
                    count++
                } else {
                    nameArray2 = condition.getTokenValue().toString()
                    val2 = condition.getFirstChildren().getFirstChildren().getFirstChildren().getTokenValue().toString()
                }
                TokenType.CHAR, TokenType.INT -> if (count == 0) {
                    val1 = condition.getFirstChildren().getTokenValue().toString()
                    count++
                } else {
                    val2 = condition.getFirstChildren().getTokenValue().toString()
                }
                TokenType.SIGN -> signType = condition.getTokenValue().toString()
                TokenType.EMPTY -> {
                }
                else -> {
                }
            }
        }

        if (type == TokenType.WHILE) {
            assembler.add(getNameWhile(randForCommand, numberIfWhile) + ":")
        }

        if (literal) {
            if (lit1 != null) {
                if (lit1 == "null") {
                    lit1 = "$0"
                    assembler.add("\tcmp\t" + lit1 + ",\t-" + addressVar[val2] + "(%rbp)")
                } else {
                    val str: StringBuilder = StringBuilder()
                    var i = lit1.length
                    while (i > 0) {
                        str.append(Integer.toHexString(lit1[i - 1].toInt()))
                        i--
                    }
                    assembler.add("\tcmp\t0x" + str + ",\t-" + addressVar[val2] + "(%rbp)")
                }
            }
            if (lit2 != null) {
                if (lit2 == "null") {
                    lit2 = "$0"
                }
                assembler.add("\tcmp\t" + lit2 + ",\t-" + addressVar[val1] + "(%rbp)")
            }
        } else {
            if (nameArray1 == null) {
                if (isNumeric(val1)) {
                    assembler.add("\tmovl\t$$val1,\t%eax")
                } else {
                    assembler.add("\tmovl\t-" + addressVar[val1] + "(%rbp),\t%eax")
                }
            } else {
                if (isNumeric(val1)) {
                    val arValue = nameArray1 + val1
                    assembler.add("\tmovl\t-" + addressVar[arValue] + "(%rbp),\t%eax")
                } else {
                    if (val1 != null) {
                        arrayToAsm(val1, nameArray1, assembler)
                    }
                }
            }
            if (nameArray2 == null) {
                if (isNumeric(val2)) {
                    assembler.add("\tcmpl\t$$val2,\t%eax")
                } else {
                    assembler.add("\tcmpl\t-" + addressVar[val2] + "(%rbp),\t%eax")
                }
            } else {
                if (isNumeric(val2)) {
                    val arValue = nameArray2 + val2
                    assembler.add("\tcmpl\t-" + addressVar[arValue] + "(%rbp),\t%eax")
                } else {
                    if (val2 != null) {
                        arrayToAsm(val2, nameArray2, assembler)
                    }
                    assembler.add("\tcmpl\t%eax,\t-" + addressVar[val2] + "(%rbp)") // val1
                }
            }
        }
        assert(signType != null)
        singType(signType, assembler, randForCommand, numberIfWhile, type)
    }

    private fun arrayToAsm(vall: String, nameArray: String?, assembler: MutableList<String>) {
        var nameArrray: String? = nameArray
        assembler.add("\tmovl    -" + addressVar[vall] + "(%rbp), %eax")
        assembler.add("\tcltd")

        val addresArray: MutableList<String>? = arrays[nameArrray]
        nameArrray += (addresArray!!.size - 1)

        assembler.add("\tmovl    -" + addressVar[nameArrray] + "(%rbp,%rax,4), %eax")
    }


    private fun singType(signType: String?, assembler: MutableList<String>, randForCommand: Int, numberIfWhile: Int, type: TokenType) {
        when (signType) {
            ">" -> when (type) {
                TokenType.IF -> assembler.add("\tjle\t" + getNameIfNext(randForCommand, numberIfWhile))
                TokenType.WHILE -> assembler.add("\tjg\t" + getNameWhileNext(randForCommand, numberIfWhile))
                else -> {
                }
            }
            "<" -> when (type) {
                TokenType.IF -> assembler.add("\tjge\t" + getNameIfNext(randForCommand, numberIfWhile))
                TokenType.WHILE -> assembler.add("\tjl\t" + getNameWhileNext(randForCommand, numberIfWhile))
                else -> {
                }
            }
            "<=" -> when (type) {
                TokenType.IF -> assembler.add("\tjg\t" + getNameIfNext(randForCommand, numberIfWhile))
                TokenType.WHILE -> assembler.add("\tjle\t" + getNameWhileNext(randForCommand, numberIfWhile))
                else -> {
                }
            }
            "==" -> when (type) {
                TokenType.IF -> assembler.add("\tjne\t" + getNameIfNext(randForCommand, numberIfWhile))
                TokenType.WHILE -> assembler.add("\tje\t" + getNameWhileNext(randForCommand, numberIfWhile))
                else -> {
                }
            }
            "!=" -> when (type) {
                TokenType.IF -> assembler.add("\tje\t" + getNameIfNext(randForCommand, numberIfWhile))
                TokenType.WHILE -> assembler.add("\tjne\t" + getNameWhileNext(randForCommand, numberIfWhile))
                else -> {
                }
            }
            ">=" -> when (type) {
                TokenType.IF -> assembler.add("\tjl\t" + getNameIfNext(randForCommand, numberIfWhile))
                TokenType.WHILE -> assembler.add("\tjge\t" + getNameWhileNext(randForCommand, numberIfWhile))
                else -> {
                }
            }
        }
    }

    private fun createArray(array: Node): MutableList<String> {
        var countArray = 0

        var body: MutableList<String> = ArrayList()

        for (arChild in array.getListChild()) {
            when (arChild.getTokenType()) {
                TokenType.INT -> try {
                    countArray = arChild.getFirstChildren().getTokenValue() as Int
                } catch (e: ClassCastException) {
                    println("Индекс массива не может быть переменной")
                    exitProcess(0)
                }
                TokenType.ARRAY_BODY -> body = arrayBody(arChild, countArray)
                else -> {
                }
            }
        }
        arrays[nameVariable!!] = body

        val asArray: MutableList<String> = ArrayList()

        var i = 0
        while (i < body.size) {

            val nameVar = nameVariable + i
            setVar(nameVar, TokenType.INT)
            asArray.add("\tmovl    $" + body[i] + " , -" + addressVar[nameVar] + "(%rbp)")
            i++
        }
        return asArray
    }

    private fun arrayBody(body: Node, arCounter: Int): MutableList<String> {
        val value: MutableList<String> = ArrayList()
        var realCounter = 0

        for (arBody in body.getListChild()) {
            if (arCounter + 1 > realCounter) {
                when (arBody.getTokenType()) {
                    TokenType.INT -> {
                        value.add(arBody.getFirstChildren().getTokenValue().toString())
                        realCounter++
                    }
                    else -> {
                    }
                }
            } else {
                println("Количество объявленных элементов больше величины массива")
                exitProcess(0)
            }
        }
        return value
    }

    private fun arrayAssigment(assigment: Node, commandAssembler: MutableList<String>) {

        var nameAsVar: String? = null

        for (arAssigment in assigment.getListChild()) {
            when (arAssigment.getTokenType()) {
                TokenType.NUMBER -> {
                    val num = arAssigment.getTokenValue().toString()
                    nameAsVar = nameVariable + num
                }
                TokenType.ASSIGNMENT -> if (arAssigment.getFirstChildren().getTokenType() == TokenType.NUMBER) {
                    val value = arAssigment.getFirstChildren().getTokenValue().toString()
                    commandAssembler.add("\tmovl    $" + value + ", -" + addressVar[nameAsVar] + "(%rbp)")
                }
                else -> {
                }
            }
        }
    }

    private fun assigment(number: Node, commandAssembler: MutableList<String>) {
        var arguments: MutableList<String> = ArrayList()
        var nameFunction: String? = null

        try {
            if (number.getFirstChildren().getTokenType() == TokenType.CALL_FUNCTION) {
                for (args in number.getFirstChildren().getListChild()) {
                    when (args.getTokenType()) {
                        TokenType.NAME -> nameFunction = args.getTokenValue().toString()
                        TokenType.ARG_LIST -> arguments = argList(args)!!
                        else -> {
                        }
                    }
                }
                when (arguments.size) {
                    0 -> {
                        commandAssembler.add("\tmovl\t$0,\t%eax")
                        commandAssembler.add("\tcall\t$nameFunction")
                        commandAssembler.add("\tmovl\t%eax,\t-" + addressVar[nameVariable] + "(%rbp)")
                    }
                    1 -> if (isNumeric(arguments[0])) {
                        commandAssembler.add("\tmovl\t$" + arguments[0] + ",\t%edi")
                        commandAssembler.add("\tcall\t$nameFunction")
                        commandAssembler.add("\tmovl\t%eax,\t-" + addressVar[nameVariable] + "(%rbp)")
                    } else {
                        commandAssembler.add("\tmovl\t-" + addressVar[arguments[0]] + "(%rbp), %eax")
                        commandAssembler.add("\tmovl\t%eax, %edi")
                        commandAssembler.add("\tcall\t$nameFunction")
                        commandAssembler.add("\tmovl\t%eax,\t-" + addressVar[nameVariable] + "(%rbp)")
                    }
                    2 -> {
                        if (isNumeric(arguments[0])) {
                            commandAssembler.add("\tmovl\t$" + arguments[0] + ", %edi")
                        } else {
                            commandAssembler.add("\tmovl\t-" + addressVar[arguments[0]] + "(%rbp), %eax")
                            commandAssembler.add("\tmovl\t%eax, %edi")
                        }
                        if (isNumeric(arguments[1])) {
                            commandAssembler.add("\tmovl\t$" + arguments[1] + ", %esi")
                        } else {
                            commandAssembler.add("\tmovl\t-" + addressVar[arguments[0]] + "(%rbp), %eax")
                            commandAssembler.add("\tmovl\t%eax, %esi")
                        }
                        commandAssembler.add("\tcall\t$nameFunction")
                        commandAssembler.add("\tmovl\t%eax,\t-" + addressVar[nameVariable] + "(%rbp)")
                    }
                    else -> println("Передввать можно не более двух аргументов")
                }
            } else {
                assigmentElse(number, commandAssembler)
            }
        } catch (e: IndexOutOfBoundsException) {
            assigmentElse(number, commandAssembler)
        }
    }

    private fun argList(argList: Node): MutableList<String>? {
        val arguments: MutableList<String> = ArrayList()
        for (arg in argList.getListChild()) {
            when (arg.getTokenType()) {
                TokenType.EMPTY -> return null
                TokenType.NUMBER, TokenType.NAME -> arguments.add(arg.getTokenValue().toString())
                else -> {
                }
            }
        }
        return arguments
    }

    private fun assigmentElse(number: Node, commandAssembler: MutableList<String>) {
        if (number.getListChild().size > 0) {
            for (numRec in number.getListChild()) {
                assigment(numRec, commandAssembler)
            }
        } else {
            if (number.getTokenType() == TokenType.LITERAL) {

                val str = StringBuilder()
                val literal = number.getTokenValue().toString()
                var i = literal.length
                while (i > 0) {
                    str.append(Integer.toHexString(literal[i - 1].toInt()))
                    i--
                }
                commandAssembler.add("\tmovabsq\t$0x$str, %rax")
                commandAssembler.add("\tmovq\t%rax, -" + addressVar[nameVariable] + "(%rbp)")
            }
            when (number.getParent().getParent().getTokenType()) {
                TokenType.ARRAY -> {
                    when (number.getTokenType()) {
                        //a[3]
                        TokenType.NUMBER -> {
                            val arNum = number.getTokenValue().toString()
                            val varName = number.getParent().getParent().getParent().getTokenValue().toString()
                            val value = varName + arNum
                            commandAssembler.add("\tmovl\t-" + addressVar[value] + "(%rbp), %eax")
                            commandAssembler.add("\tmovl\t%eax, -" + addressVar[nameVariable] + "(%rbp)")
                        }
                        //a[b]
                        TokenType.NAME -> {
                            val varNameAssign = number.getTokenValue().toString()

                            commandAssembler.add("\tmovl\t-" + addressVar[varNameAssign] + "(%rbp), %eax")
                            commandAssembler.add("\tcltd")

                            var nameArray = number.getParent().getParent().getParent().getTokenValue().toString()
                            val addresArray: List<String>? = arrays[nameArray]
                            nameArray += (addresArray!!.size - 1)

                            commandAssembler.add("\tmovl\t-" + addressVar[nameArray] + "(%rbp,%rax,4), %eax")
                            commandAssembler.add("\tmovl\t%eax, -" + addressVar[nameVariable] + "(%rbp)")
                        }
                        else -> {
                        }
                    }
                }
                TokenType.ASSIGNMENT -> when (number.getTokenType()) {
                    TokenType.NUMBER -> commandAssembler.add("\tmovl\t$" + number.getTokenValue() + ", -" + addressVar[nameVariable] + "(%rbp)")
                    TokenType.NAME -> {
                        val valueAsName = number.getTokenValue().toString()
                        commandAssembler.add("\tmovl\t-" + addressVar[valueAsName] + "(%rbp), %eax")
                        commandAssembler.add("\tmovl\t%eax,\t-" + addressVar[nameVariable] + "(%rbp)")
                    }
                    else -> {
                    }
                }
                TokenType.DIVISION, TokenType.MINUS, TokenType.PLUS, TokenType.MULTIPLICATION -> assemblerMath(number, commandAssembler, number.getParent().getParent().getTokenType())
                else -> {
                }
            }
        }
    }

    private fun assemblerMath(number: Node, commandAssembler: MutableList<String>, type: TokenType) {
        if (node != 1) {
            val num1 = number.getParent().getParent().getListChild()[0].getFirstChildren().getTokenValue().toString()
            val num2 = number.getParent().getParent().getListChild()[1].getFirstChildren().getTokenValue().toString()

            if (type == TokenType.DIVISION) {
                if (isNumeric(num1) && isNumeric(num2)) {
                    val one = num1.toInt()
                    val two = num2.toInt()

                    val result = (one / two).toString()
                    assembler.add("\tmovl\t$" + result + ", -" + addressVar[nameVariable] + "(%rbp)")
                } else {
                    if (isNumeric(num1)) {
                        setVar("numDiv1", TokenType.INT)
                        assembler.add("\tmovl\t$" + num1 + ", -" + addressVar["numDiv1"] + "(%rbp)")
                        assembler.add("\tmovl\t-" + addressVar["numDiv1"] + "(%rbp), %eax")
                    } else {
                        assembler.add("\tmovl\t-" + addressVar[num1] + "(%rbp), %eax")
                        assembler.add("\tcltd")
                    }

                    if (isNumeric(num2)) {
                        setVar("numDiv2", TokenType.INT)
                        assembler.add("\tmovl\t$" + num2 + ", -" + addressVar["numDiv2"] + "(%rbp)")
                        assembler.add("\tidivl\t-" + addressVar["numDiv2"] + "(%rbp)")
                    } else {
                        assembler.add("\tcltd")
                        assembler.add("\tidivl\t-" + addressVar[num2] + "(%rbp)")
                    }
                    assembler.add("\tmovl\t%eax, -" + addressVar[nameVariable] + "(%rbp)")
                }
            } else if (isNumeric(num1)) {
                commandAssembler.add("\tmovl\t$$num1, %edx")
            } else {
                commandAssembler.add("\tmovl\t-" + addressVar[num1] + "(%rbp), %edx")
            }

            when (type) {
                TokenType.MINUS -> {
                    if (isNumeric(num2)) {
                        commandAssembler.add("\tsubl\t$$num2, %edx")
                    } else {
                        commandAssembler.add("\tsubl\t-" + addressVar[num2] + "(%rbp), %edx")
                    }
                    commandAssembler.add("\tmovl\t%edx, -" + addressVar[nameVariable] + "(%rbp)")
                }
                TokenType.PLUS -> {
                    if (isNumeric(num2)) {
                        commandAssembler.add("\taddl\t$$num2, %edx")
                    } else {
                        commandAssembler.add("\taddl\t-" + addressVar[num2] + "(%rbp), %edx")
                    }
                    commandAssembler.add("\tmovl\t%edx, -" + addressVar[nameVariable] + "(%rbp)")
                }
                TokenType.MULTIPLICATION -> {
                    if (isNumeric(num2)) {
                        commandAssembler.add("\tmovl\t$$num2, %eax")
                    } else {
                        commandAssembler.add("\tmovl\t-" + addressVar[num2] + "(%rbp), %eax")
                    }
                    commandAssembler.add("\tmull\t%edx")
                    commandAssembler.add("\tmovl\t%eax, -" + addressVar[nameVariable] + "(%rbp)")
                }
                else -> {
                }
            }
            node++
        }
    }

    private fun scanfBody(command: Node, literal: MutableList<String>, commandAsm: MutableList<String>) {

        literal.add("." + getNameLC() + ":")
        literal.add("\t.string \"" + command.getFirstChildren().getTokenValue().toString() + "\"")

        val names: MutableList<String> = ArrayList()

        if (command.getListChild().size > 1) {
            for (printfBody in command.getListChild()) {
                when (printfBody.getTokenType()) {
                    TokenType.INT, TokenType.CHAR, TokenType.DOUBLE -> {
                        val name = printfBody.getFirstChildren().getTokenValue().toString()
                        names.add(name)
                    }
                    else -> {
                    }
                }
            }
        }

        if (names.size == 1) {
            commandAsm.add(0, "\txorl\t%eax,\t%eax")
            commandAsm.add(1, "\tmovq\t$." + getNameLC() + ",\t%rdi")
            commandAsm.add(2, "\tleaq\t-" + addressVar[names[0]] + "(%rbp),\t%rsi")
        } else if (names.size > 1) {
            println("Принимать можно не более одного значения")
            exitProcess(0)
        }
    }

    private fun printfBody(command: Node, literal: MutableList<String>, commandAsm: MutableList<String>) {
        literal.add("." + getNameLC() + ":")
        literal.add("\t.string \"" + command.getFirstChildren().getTokenValue().toString() + "\"")

        val names: MutableList<String> = ArrayList()
        val type: MutableList<TokenType> = ArrayList()

        if (command.getListChild().size > 1) {
            for (printfBody in command.getListChild()) {
                when (printfBody.getTokenType()) {
                    TokenType.INT, TokenType.CHAR, TokenType.DOUBLE -> {
                        type.add(printfBody.getTokenType())
                        names.add(printfBody.getFirstChildren().getTokenValue().toString())
                    }
                    else -> {
                    }
                }
            }
        }


        when (names.size) {
            0 -> commandAsm.add(0, "\tmovl\t$." + getNameLC() + ",\t%edi")
            1 -> when (type[0]) {
                TokenType.INT -> {
                    commandAsm.add(0, "\tmovl\t-" + addressVar[names[0]] + "(%rbp),\t%eax")
                    commandAsm.add(1, "\tmovl\t%eax,\t%esi")
                    commandAsm.add(2, "\tmovl\t$." + getNameLC() + ",\t%edi")
                }
                TokenType.CHAR -> {
                    commandAsm.add(0, "\tleaq\t-" + addressVar[names[0]] + "(%rbp),\t%rax")
                    commandAsm.add(1, "\tmovq\t%rax,\t%rsi")
                    commandAsm.add(2, "\tmovl\t$." + getNameLC() + ",\t%edi")
                }
                else -> {
                }
            }
            2 -> if ((type[0] == type[0]) && (type[0] == TokenType.INT)) {
                commandAsm.add(0, "\tmovl\t-" + addressVar[names[1]] + "(%rbp),\t%edx")
                commandAsm.add(1, "\tmovl\t-" + addressVar[names[0]] + "(%rbp),\t%eax")
                commandAsm.add(2, "\tmovl\t%eax,\t%esi")
                commandAsm.add(3, "\tmovl\t$." + getNameLC() + ",\t%edi")
            } else {
                println("Можно вывести только два INT")
                exitProcess(0)
            }
            else -> {
                println("Такое количество параметров вывода не завезли")
                exitProcess(0)
            }
        }
    }
}