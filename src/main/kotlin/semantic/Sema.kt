package semantic

import idTable.Variable
import lexer.Token
import lexer.TokenType
import parser.Node
import java.util.*
import kotlin.math.roundToInt
import kotlin.system.exitProcess

class Sema() {

    private var nameFunction: String? = null
    private var nameVariable: String? = null
    private var assigment = false
    private var returned = false
    private lateinit var buffer: Node
    lateinit var treeSema: Node
    private var functionCount: MutableMap<String, Int>
    private var idTableSema: MutableMap<String, MutableList<Variable>>? = null
    private var functionParams: MutableMap<String, List<TokenType>>
    private var arrays: MutableMap<String, String>
    private var subLevel: MutableMap<Int, Char>
    private var level: Int

    init {
        this.subLevel = HashMap()
        this.level = 0
        this.functionCount = HashMap()
        this.arrays = HashMap()
        this.functionParams = HashMap()
    }

    @Throws(CloneNotSupportedException::class)
    constructor(tree: Node, idTableSema: MutableMap<String, MutableList<Variable>>?) : this() {
        this.treeSema = tree.clone()
        this.idTableSema = idTableSema
        this.subLevel = HashMap()
        this.level = 0
        this.functionCount = HashMap()
        this.arrays = HashMap()
        this.functionParams = HashMap()
        analyze()
    }

    private fun checkDouble(str: String): Boolean {
        try {
            val d = str.toInt().toDouble()
        } catch (nfe: NumberFormatException) {
            return false
        } catch (nfe: NullPointerException) {
            return false
        }
        return true
    }

    private fun addSubLevel(level: Int) {
        var subLvl: Char? = subLevel[level]

        when {
            subLvl == null -> {
                subLevel[level] = 'a'
            }
            level == 0 -> {
                subLevel[level] = 'a'
            }
            else -> {
                subLvl = (subLvl + 1)
                subLevel[level] = subLvl
            }
        }
    }

    @Throws(CloneNotSupportedException::class)
    fun analyze() {
        for (child: Node in treeSema.getListChild()) {
            if (child.getTokenType() == TokenType.FUNCTION) {
                body(child)
            }
        }
    }

    @Throws(CloneNotSupportedException::class)
    private fun body(childFunc: Node) {
        addSubLevel(level)
        var name = ""

        for (child: Node in childFunc.getListChild()) {
            when (child.getTokenType()) {
                TokenType.BODY_THEN, TokenType.BODY_ELSE, TokenType.BODY -> bodyRec(child)
                TokenType.NAME -> name = bodyName(child, childFunc)
                TokenType.PARAMS_LIST -> paramsCounter(child, name)
                else -> {
                }
            }
        }
    }

    private fun paramsCounter(child: Node, name: String) {
        if (child.getFirstChildren().getTokenType() != TokenType.EMPTY) {
            var countParams = 0
            for (params in child.getListChild()) {
                if (params.getTokenType() === TokenType.PARAM) {
                    countParams++
                }
            }
            functionCount[name] = countParams
            parasList(child, name)
        } else {
            functionCount[name] = 0
        }
    }

    private fun parasList(params: Node, name: String) {

        val parameters: ArrayList<TokenType> = ArrayList()

        for (param: Node in params.getListChild()) {
            val type: TokenType = param.getFirstChildren().getFirstChildren().getTokenType()
            parameters.add(type)
        }
        functionParams[name] = parameters
    }


    @Throws(CloneNotSupportedException::class)
    fun bodyName(child: Node, childFunc: Node): String {
        buffer = child.clone()
        val name: String = childFunc.getTokenValue().toString()
        val lvl: String = level.toString() + subLevel[level].toString()
        val type: TokenType? = getTokenType(lvl, name)
        typeCheck(type, childFunc)
        if (type != null) {
            child.changeNode(type)
        }
        child.setLeft(buffer)
        nameFunction = name
        functionCount[name] = 0
        return name
    }

    @Throws(CloneNotSupportedException::class)
    fun bodyRec(body: Node) {

        level++
        addSubLevel(level)

        for (childBody: Node in body.getListChild()) {
            bodyCommand(childBody)
        }
    }

    @Throws(CloneNotSupportedException::class)
    fun bodyCommand(childBody: Node) {
        when (childBody.getTokenType()) {
            TokenType.COMMAND -> for (childCommand: Node in childBody.getListChild()) {
                commands(childCommand)
            }
            TokenType.CALL_FUNCTION -> function(childBody)
            TokenType.EMPTY -> level--
            else -> {
            }
        }
    }

    @Throws(CloneNotSupportedException::class)
    fun commands(childCommand: Node) {
        when (childCommand.getTokenType()) {
            TokenType.BODY_THEN, TokenType.BODY_ELSE, TokenType.BODY -> bodyRec(childCommand)
            TokenType.NAME -> commandName(childCommand)
            TokenType.NUMBER -> {
                if (returned) {
                    nameVariable = nameFunction
                    commandRec(childCommand)
                }
                returned = false
            }
            TokenType.RETURN -> returned = true
            TokenType.ASSIGNMENT -> {
                assigment = true
                assigment(childCommand)
            }
            TokenType.ARRAY -> array(childCommand)
            TokenType.ARRAYASSIGMENT -> arrayAssigment(childCommand)
            //TokenType.CONDITION -> condition(childCommand) //////// может не работать, не реалиховано нормально
            TokenType.CALL_FUNCTION -> {
                assigment = false
                commandCallFunction(childCommand)
            }
            else -> commandRec(childCommand)
        }
    }

    @Throws(CloneNotSupportedException::class)
    fun assigment(ass: Node) {
        if (ass.getFirstChildren().getTokenType() == TokenType.CALL_FUNCTION) {
            assigment = false
            commandCallFunction(ass)
        } else {
            commandRec(ass)
        }
    }

    @Throws(CloneNotSupportedException::class)
    fun commandCallFunction(childCommand: Node) {
        function(childCommand)
    }

    @Throws(CloneNotSupportedException::class)
    fun commandName(childCommand: Node) {
        //приведение типов
        buffer = childCommand.clone()
        val lvl: String = level.toString() + subLevel[level].toString()

        if (assigment) {

            val type1: TokenType = getTokenType(lvl, nameVariable!!)!!
            typeCheck(type1, childCommand)

            nameVariable = childCommand.getTokenValue().toString()
            val type2: TokenType = getTokenType(lvl, nameVariable!!)!!

            typeCheck(type2, childCommand)

            val type: TokenType = typeCheckType(type1, type2)
            childCommand.changeNode(type)
        } else {
            nameVariable = childCommand.getTokenValue().toString()
            val type = getTokenType(lvl, nameVariable!!)
            typeCheck(type, childCommand)
            // проверка типов
            if (returned) {
                val typeFunction = getTokenType("0a", nameFunction!!)
                if (typeFunction === TokenType.VOID || type !== typeFunction) {
                    println("SEMA: void не может возвращать значения или возвращаемый тип не равен типу функции LOC<" + childCommand.getValue()!!.row + ":" + childCommand.getValue()!!.col + ">")
                    exitProcess(0)
                }
            }
            if (type != null) {
                childCommand.changeNode(type)
            }
        }
        returned = false
        childCommand.setLeft(buffer)
    }

    private fun typeCheckType(inn: TokenType, outt: TokenType): TokenType {
        var out: TokenType = outt
        if (inn == out) {
            return out
        }
        when (inn) {
            TokenType.INT -> out = convertInt(out)
            TokenType.DOUBLE -> out = convertDouble(out)
            TokenType.CHAR -> out = convertChar(out)
            else -> {
            }
        }
        return out
    }

    private fun convertInt(outt: TokenType): TokenType {
        var out: TokenType = outt
        when (out) {
            TokenType.CHAR -> out = TokenType.INTTOCHAR
            TokenType.DOUBLE -> out = TokenType.INTTODOUBLE
            else -> {
            }
        }
        return out
    }

    private fun convertDouble(outt: TokenType): TokenType {
        var out: TokenType = outt
        when (out) {
            TokenType.INT -> out = TokenType.DOUBLETOINT
            TokenType.CHAR -> out = TokenType.DOUBLETOCHAR
            else -> {
            }
        }
        return out
    }

    private fun convertChar(outt: TokenType): TokenType {
        var out: TokenType = outt
        when (out) {
            TokenType.INT -> out = TokenType.CHARTOINT
            TokenType.DOUBLE -> out = TokenType.CHARTODOUBLE
            else -> {
            }
        }
        return out
    }

    private fun typeCheck(type: TokenType?, childCommand: Node) {
        if (type == null) {
            println("SEMA: переменная не была объявлена LOC<" + childCommand.getValue()!!.row + ":" + childCommand.getValue()!!.col + ">")
            exitProcess(0)
        }
    }

    // проверка на double не совершенна
    @Throws(CloneNotSupportedException::class)
    fun array(array: Node) {
        val value = array.getFirstChildren().getTokenValue().toString()

        val reg = """[\\p{L}| ]+""".toRegex()

        when {
            value.matches(reg) -> {
                arrays[nameVariable.toString()] = value
            }
            checkDouble(value) -> {
                arrays[nameVariable.toString()] = value
            }
            else -> {
                println("SEMA: индекс массива должен быть целочисленным")
                exitProcess(0)
            }
        }
        commandRec(array)
    }

    @Throws(CloneNotSupportedException::class)
    fun arrayAssigment(arrayAssigment: Node) {
        for (array: Node in arrayAssigment.getListChild()) {
            when (array.getTokenType()) {
                TokenType.NUMBER, TokenType.NAME -> nameArrayAssigment(array)
                TokenType.ASSIGNMENT -> assigment = true
                else -> if (assigment) {
                    commandRec(array)
                }
            }
        }
    }

    @Throws(CloneNotSupportedException::class)
    fun nameArrayAssigment(array: Node) {
        if (assigment) {
            commandRec(array)
        } else {
            var value = 0
            try {
                value = array.getTokenValue() as Int
            } catch (e: ClassCastException) {
                println("SEMA: переменная ")
            }
            val count: Int = arrays[nameVariable]!!.toInt()
            if (value > count) {
                println("SEMA: выход за пределы массива LOC<" + array.getValue()!!.row + ":" + array.getValue()!!.col + ">")
                exitProcess(0)
            }
        }
    }

    @Throws(CloneNotSupportedException::class)
    fun function(function: Node) {
        var name: String? = null
        for (funn: Node in function.getListChild()) {
            when (funn.getTokenType()) {
                TokenType.NAME -> name = nameFunction(funn)
                TokenType.ARG_LIST -> argListFunction(funn, name)
                else -> {
                }
            }
        }

    }

    @Throws(CloneNotSupportedException::class)
    fun argListFunction(funn: Node, name: String?) {
        var countArgs = 0
        for (args in funn.getListChild()) {
            if (args.getTokenType() == TokenType.EMPTY) {
                break
            }
            countArgs++
        }
        val count: Int
        try {
            count = functionCount[name]!!
        } catch (e: NullPointerException) {
            println("SEMA: функция должна быть описана выше main LOC<" + funn.getValue()!!.row + ":" + funn.getValue()!!.col + ">")
            exitProcess(0)
        }
        if (count != countArgs) {
            println("SEMA: разное количество принимаемых и/или отправляемых аргументов LOC<" + funn.getValue()!!.row + ":" + funn.getValue()!!.col + ">")
            exitProcess(0)
        }

        commandRec(funn)

        if (countArgs > 0) {
            val types: MutableList<TokenType> = ArrayList()
            for (typeARGS: Node in funn.getListChild()) {
                types.add(typeARGS.getTokenType())
            }
            val types1: List<TokenType>? = functionParams[name]
            var i = 0
            while (i < types1!!.size) {
                if (types[i] != types1[i]) {
                    println("SEMA: разные типы отправляемых значений в функцию. Ожидался " + types1[i] + ", а на входе " + types[i] + " LOC<" + funn.getValue()!!.row + ":" + funn.getValue()!!.col + ">")
                    exitProcess(0)
                }
                i++
            }
        }
    }

    @Throws(CloneNotSupportedException::class)
    fun nameFunction(funn: Node): String {
        buffer = funn.clone()
        val name = funn.getTokenValue().toString()
        val lvl = "0a"
        val type = getTokenType(lvl, name)
        typeCheck(type, funn)
        if (type != null) {
            funn.changeNode(type)
        }
        funn.setLeft(buffer)
        return name
    }

    @Throws(CloneNotSupportedException::class)
    fun commandRec(command: Node?) {
        if (command != null) {
            if (command.getListChild().size > 0) {
                for (recCommand in command.getListChild()) {
                    commandRec(recCommand)
                }
            } else {
                when (command.getTokenType()) {
                    TokenType.NUMBER -> {
                        buffer = command.clone()
                        if (assigment) {
                            convertNumber(command)
                        } else {
                            checkTypeNumber(command)
                        }
                    }
                    TokenType.CHAR -> {
                        buffer = command.clone()
                        if (assigment) {
                            convertChar(command)
                        } else {
                            checkTypeChar(command)
                        }
                    }
                    TokenType.NAME -> commandRecName(command)
                    TokenType.SIGN -> {
                        assigment = true
                        assigment = false
                    }
                    TokenType.EMPTY -> assigment = false
                    else -> {
                    }
                }
            }
        }
    }

    @Throws(CloneNotSupportedException::class)
    fun commandRecName(command: Node) {
        buffer = command.clone()
        val name = command.getTokenValue().toString()
        val lvl: String = level.toString() + subLevel[level].toString()
        val type = getTokenType(lvl, name)
        typeCheck(type, command)
        if (type != null) {
            command.changeNode(type)
        }
        command.setLeft(buffer)
    }

    private fun checkTypeChar(command: Node) {
        if (command.getTokenValue() is Char) {
            command.changeNode(TokenType.CHAR)
            command.setLeft(buffer)
        }
    }

    private fun convertChar(command: Node) {
        val lvl: String = level.toString() + subLevel[level].toString()
        val type: TokenType? = getTokenType(lvl, nameVariable!!)
        var value: String = command.getTokenValue().toString()
        val newValue: Token<*>

        when (type) {
            TokenType.CHAR -> if (command.getTokenValue() is Char) {
                command.changeNode(TokenType.CHAR)
                command.setLeft(buffer)
            }
            TokenType.INT -> {
                value = convertTypeValue(TokenType.CHAR, TokenType.INT, value)
                newValue = Token(TokenType.NUMBER, Integer.valueOf(value))
                buffer.setValue(newValue)
                command.changeNode(TokenType.INT)
                command.setLeft(buffer)
            }
            TokenType.DOUBLE -> {
                value = convertTypeValue(TokenType.CHAR, TokenType.DOUBLE, value)
                newValue = Token(TokenType.NUMBER, java.lang.Double.valueOf(value))
                buffer.setValue(newValue)
                command.changeNode(TokenType.DOUBLE)
                command.setLeft(buffer)
            }
            else -> {
            }
        }
    }

    private fun checkTypeNumber(command: Node) {
        if (command.getTokenValue() is Int) {
            command.changeNode(TokenType.INT)
            command.setLeft(buffer)
        }
        if (command.getTokenValue() is Double) {
            command.changeNode(TokenType.DOUBLE)
            command.setLeft(buffer)
        }
    }

    private fun convertNumber(command: Node) {
        val lvl: String = level.toString() + subLevel[level].toString()
        val type = getTokenType(lvl, nameVariable!!)
        var value = command.getTokenValue().toString()

        when (type) {
            TokenType.CHAR -> {
                println("SEMA: к char присваивать можно только char")
                exitProcess(0)
            }
            TokenType.INT -> {
                if (command.getTokenValue() !is Int) {
                    value = convertTypeValue(TokenType.DOUBLE, TokenType.INT, value)
                    val newValue: Token<*> = Token(TokenType.NUMBER, value.toInt())
                    buffer.setValue(newValue)
                }
                command.changeNode(TokenType.INT)
                command.setLeft(buffer)
            }
            TokenType.DOUBLE -> {
                if (command.getTokenValue() !is Double) {
                    value = convertTypeValue(TokenType.INT, TokenType.DOUBLE, value)
                    val newValue: Token<*> = Token(TokenType.NUMBER, value.toDouble())
                    buffer.setValue(newValue)
                }
                command.changeNode(TokenType.DOUBLE)
                command.setLeft(buffer)
            }
            else -> {
            }
        }
    }

    //tested
    private fun convertTypeValue(inn: TokenType, out: TokenType, valuee: String): String {
        var value = valuee
        if (inn == out) {
            return value
        } else {
            when (inn) {
                TokenType.INT -> value = convertValueInt(out, value)
                TokenType.DOUBLE -> value = convertValueDouble(out, value)
                TokenType.CHAR -> value = convertValueChar(out, value)
                else -> {
                }
            }
        }
        return value
    }

    private fun convertValueInt(out: TokenType, value: String): String {
        when (out) {
            TokenType.DOUBLE -> value.toInt().toString()
            else -> {
            }
        }
        return value
    }

    private fun convertValueDouble(out: TokenType, value: String): String {
        when (out) {
            TokenType.INT -> value.toDouble().roundToInt().toString()
            else -> {
            }
        }
        return value
    }

    private fun convertValueChar(out: TokenType, value: String): String {
        when (out) {
            TokenType.INT -> value[0].toInt().toString()
            TokenType.DOUBLE -> value[0].toDouble().toString()
            else -> {
            }
        }
        return value
    }

    // tested
    private fun getTokenType(lvl: String, name: String): TokenType? {
        val variableList: MutableList<Variable> = idTableSema?.get(name) ?: return null

        for (variable in variableList) {
            if (variable.getValue() == lvl) {
                return variable.getTokenType()
            }
        }

        var checkLvl = false
        var nameLvl = lvl[1]
        for (variable in variableList) {
            if (variable.getValue()!![1] == nameLvl) {
                checkLvl = true
                break
            }
        }

        if (!checkLvl) {
            nameLvl = (lvl[1].toInt() - 1).toChar()
            for (variable in variableList) {
                if (variable.getValue()!![1] == nameLvl) {
                    checkLvl = true
                    break
                }
            }
        }
        if (!checkLvl) {
            nameLvl = (lvl[1].toInt() - 2).toChar()
            for (variable in variableList) {
                if (variable.getValue()!![1] == nameLvl) {
                    checkLvl = true
                    break
                }
            }
        }
        if (!checkLvl) {
            nameLvl = (lvl[1].toInt() - 3).toChar()
            for (variable in variableList) {
                if (variable.getValue()!![1] == nameLvl) {
                    checkLvl = true
                    break
                }
            }
        }

        if (!checkLvl) {
            return null
        }

        val mass = IntArray(variableList.size)
        var i = 0
        while (i < variableList.size) {
            mass[i] = variableList[i].getValue()!!.compareTo(lvl)
            i++
        }

        var indexOfMax = 0
        var j = 0
        while (j < mass.size) {
            if (mass[j] > mass[indexOfMax]) {
                indexOfMax = j
            }
            j++
        }
        return variableList[indexOfMax].getTokenType()
    }

    @Throws(CloneNotSupportedException::class)
    fun condition(condition: Node) {

        // TODO: проверка на тип массива и после какие то действия

        val types = ArrayList<TokenType>()

        commandRec(condition)

        for (type: Node in condition.getListChild()) {
            if (type.getTokenType() != TokenType.SIGN) {
                types.add(type.getTokenType())
            }
        }

        if (types[0] != types[1]) {
            println("SEMA : переменные в условии должны быть одного типа")
            exitProcess(0)
        }
    }
}
