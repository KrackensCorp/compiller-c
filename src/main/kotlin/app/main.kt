package app

import buffer.Buffer
import codeGen.CodeGen
import idTable.IdTable
import lexer.Lexer
import lexer.Token
import lexer.TokenType
import parser.Node
import parser.Parser
import semantic.Sema
import java.io.FileReader
import java.io.FileWriter
import java.io.IOException
import java.io.Reader
import java.net.URLEncoder
import kotlin.system.exitProcess

var option: String? = null
lateinit var inputFile: String
var astPath: String? = null
var astDefaultPath: String = "./tmp/ast.dot"
var semaAstDefaultPath: String = "./tmp/semaAst.dot"
const val DEFAULT_INPUT_FILE: String = "./examples/min_max_array.c"

fun main(args: Array<String>) {
    try {
        processingArguments(args)
        compiler()
    } catch (e: ExceptionCommand) {
        e.printStackTrace()
    }
    println("Done!")
}

fun compiler() {
    val fileReader: Reader
    try {
        if (!checkFile()) {
            println("Расширение файла должно быть .c")
            exitProcess(0)
        }
        fileReader = FileReader(inputFile)
    } catch (e: IOException) {
        println("Не смогли открыть файл: $inputFile")
        exitProcess(0)
    }

    if (option == null) {
        compile(fileReader, inputFile)
    } else {
        when (option) {
            "--dump-tokens" -> {
                dumpTokens(fileReader)
            }
            "--dump-ast" -> {
                if (astPath != null) {
                    dumpAst(fileReader, astPath!!, semaAstDefaultPath)
                } else {
                    dumpAst(fileReader, astDefaultPath, semaAstDefaultPath)
                }
            }
            "--dump-asm" -> {
                dumpAsm(fileReader)
            }
        }
    }
    fileReader.close()
}

fun compile(fileReader: Reader, inputFile: String?) {
    val buffer = Buffer(fileReader)
    val lexer = Lexer(buffer)
    val parser = Parser(lexer)
    val programTree: Node = parser.parseProgram()
    val idTable = IdTable(programTree)
    val sema = Sema(programTree, idTable.getIdTable())

    getAstParent(sema.treeSema)

    val codeGen = CodeGen(programTree)
    val nameAsm: StringBuilder = StringBuilder(inputFile)

    if (inputFile != null) {
        nameAsm.setCharAt(inputFile.length - 1, 's')
    }

    val writer = FileWriter(nameAsm.toString())

    for (str in codeGen.getAssembler()) {
        writer.write(str + System.lineSeparator())
    }
    writer.close()

    println("Имя ассемблерного файла: $nameAsm") // по умолчанию ./examples/min_max_array.s
    val nameProgram: StringBuilder = StringBuilder(inputFile)
    var command = ""
    if (inputFile != null) {
        if (System.getProperty("os.name").contains("Windows")) {     // Проверка системы
            nameProgram[inputFile.length - 1] = 'e'
            nameProgram[inputFile.length - 2] = '.'
            println("Имя исполняемого файла: $nameProgram" + "xe")
            command = "gcc -o $nameProgram" + "xe -no-pie $nameAsm"
            println("ВНИМАНИЕ: Разработчик не может гарантировать компиляцию сгенерированного " +
                    "ассемблера на ОС " + System.getProperty("os.name"))
        } else {
            nameProgram[inputFile.length - 1] = ' '
            nameProgram[inputFile.length - 2] = ' '
            command = "gcc -o $nameProgram -no-pie $nameAsm"
            println("Имя бинарного файла: $nameProgram")
        }
    }
    println("Комманда компиляции ассемблера: $command")
    Runtime.getRuntime().exec(command)
}

@Throws(CloneNotSupportedException::class)
fun dumpAsm(fileReader: Reader) {
    val buffer = Buffer(fileReader)
    val lexer = Lexer(buffer)
    val parser = Parser(lexer)
    val programTree = parser.parseProgram()
    val idTable = IdTable(programTree)
    val sema = Sema(programTree, idTable.getIdTable())

    getAstParent(sema.treeSema)

    val codeGen = CodeGen(programTree)
    var i = 0
    while (i < codeGen.getAssembler().size) {
        println(codeGen.getAssembler()[i])
        i++
    }
}

@Throws(CloneNotSupportedException::class, IOException::class)
fun dumpAst(fileReader: Reader, outAst: String, outSemaAst: String) {
    val buffer = Buffer(fileReader)
    val lexer = Lexer(buffer)
    val parser = Parser(lexer)
    val programTree: Node = parser.parseProgram()

    programTree.writeGraph(outAst)
    println("Файл дерева создан по пути$outAst")
    convertDotToUrl(outAst)

    val idTable = IdTable(programTree)
    val sema = Sema(programTree, idTable.getIdTable())

    sema.treeSema.writeGraph(outSemaAst)

    println("Файл аннотированного дерева создан по пути $outSemaAst")
    convertDotToUrl(outSemaAst)
}

fun dumpTokens(fileReader: Reader) {
    val buffer = Buffer(fileReader)
    val lexer = Lexer(buffer)
    val tokenList = mutableListOf<Token<*>?>()

    while (!lexer.peekToken()!!.match(TokenType.END)) tokenList.add(lexer.getToken())

    for (token in tokenList) {
        println(token)
    }
}

@Throws(ExceptionCommand::class)
fun processingArguments(args: Array<String>) {

    when (args.size) {
        1 -> {
            println("Компиляция без дополнительных аргументов...")
            inputFile = args[0]
        }
        2 -> {
            option = args[0]
            inputFile = args[1]
            if (option != "--dump-tokens" && option != "--dump-ast" && option != "--dump-asm") {
                throw ExceptionCommand
            }
        }
        3 -> {
            option = args[0]
            inputFile = args[1]
            astPath = args[2]
            if (option != "--dump-tokens" && option != "--dump-ast" && option != "--dump-asm") {
                throw ExceptionCommand
            }
        }
        else -> {
            println("Компиляция стандартного тестового файла без дополнительных аргументов...")
            inputFile = DEFAULT_INPUT_FILE
        }
    }
}

@Throws(IOException::class)
fun convertDotToUrl(dot: String) {
    val reader = FileReader(dot)
    var c: Int
    val test: StringBuilder = StringBuilder()

    while (reader.read().also { c = it } != -1) {
        test.append(c.toChar())
    }

    val encodedUrl: String = URLEncoder.encode(test.toString(), "UTF-8").replace("+", "%20")

    println("https://dreampuf.github.io/GraphvizOnline/#$encodedUrl")
}

fun getAstParent(tree: Node) {
    if (tree.getListChild().size != 0) {
        for (children: Node in tree.getListChild()) {
            children.setParent(tree)
            getAstParent(children)
        }
    }
}

fun checkFile(): Boolean =
        (inputFile[inputFile.length - 1] == 'c' && inputFile[inputFile.length - 2] == '.')
