package parser

import lexer.Lexer
import lexer.Token
import lexer.TokenType
import kotlin.system.exitProcess

class Parser(lexer: Lexer) {

    var lexer: Lexer = lexer
        private set

    fun parseProgram(): Node {
        val result = Node(TokenType.PROGRAM)
        try {
            var curFunction: Node
            do {
                curFunction = parseFunction()
                result.setRight(curFunction)
            } while (!curFunction.match(TokenType.EMPTY))
        } catch (e: RuntimeException) {
            val errorToken: Token<*>? = lexer.getParentToken()
            println(27.toChar() + "[31m %s LOC<%d:%d>\n".format(e.message, errorToken?.row, errorToken?.col))
            exitProcess(0)
        }
        return result
    }

    private fun parseFunction(): Node {
        val result: Node
        if (lexer.peekToken()!!.match(TokenType.END)) {
            return Node(TokenType.EMPTY)
        }

        val typeNode: Node = parseType()
        val nameToken = lexer.getToken()
        if (!nameToken!!.match(TokenType.NAME)) {
            throw RuntimeException("P: Ошибка. Должно быть имя")
        }

        val openBracketToken = lexer.getToken()
        if (!openBracketToken!!.match(TokenType.BRACKET_OPEN)) {
            throw RuntimeException("P: Ошибка. Должно быть (")
        }

        val parlistNode: Node = parseParlist()

        val closeBracketToken = lexer.getToken()
        if (!closeBracketToken!!.match(TokenType.BRACKET_CLOSE)) {
            throw RuntimeException("P: Ошибка. Должно быть )")
        }

        val openBraceToken = lexer.getToken()

        if (!openBraceToken!!.match(TokenType.BRACE_OPEN)) {
            throw RuntimeException("P: Ошибка. Должно быть  {")
        }

        val bodyNode: Node = parseBody("def")

        val closeBraceToken = lexer.getToken()
        if (!closeBraceToken!!.match(TokenType.BRACE_CLOSE)) {
            throw RuntimeException("P: Ошибка. Должно быть }")
        }

        result = Node(Token(TokenType.FUNCTION, nameToken.getTokenValue().toString()))
        result.setLeft(typeNode)
        result.setRight(Node(nameToken))
        result.setRight(parlistNode)
        result.setRight(bodyNode)

        return result
    }

    private fun parseParlist(): Node {
        val closeBracketToken = lexer.peekToken()
        if (closeBracketToken!!.match(TokenType.BRACKET_CLOSE)) {
            val emptyParList = Node(TokenType.PARAMS_LIST)
            emptyParList.setLeft(Node(TokenType.EMPTY))
            return emptyParList
        }

        val result = Node(TokenType.PARAMS_LIST)
        var commaToken: Token<*>
        do {
            val typeNode: Node = parseType()
            val nameToken = lexer.getToken()
            if (!nameToken!!.match(TokenType.NAME)) {
                throw RuntimeException("P: переменная не имеет имени")
            }
            val newParam = Node(Token(TokenType.PARAM, nameToken.getTokenValue().toString()))
            newParam.setLeft(typeNode)
            result.setRight(newParam)
            commaToken = lexer.peekToken()!!
        } while (commaToken.match(TokenType.COMMA) && commaToken === lexer.getToken())

        if (!(commaToken.match(TokenType.COMMA) || commaToken.match(TokenType.BRACKET_CLOSE))) {
            throw RuntimeException("P: ошибка закрытия аргументов функции")
        }
        return result
    }

    private fun parseBody(type: String): Node {

        val result: Node = when (type) {
            "def" -> Node(TokenType.BODY)
            "then" -> Node(TokenType.BODY_THEN)
            "else" -> Node(TokenType.BODY_ELSE)
            else -> throw IllegalStateException("Unexpected value: $type")
        }

        val closeBraceToken = lexer.peekToken()
        if (closeBraceToken!!.match(TokenType.BRACE_CLOSE)) {
            result.setLeft(Node(TokenType.EMPTY))
            return result
        }

        var command: Node = parseCommand()
        result.setLeft(command)
        do {
            if (checkExceptionSemicolon(command)) {
                val semicolonToken = lexer.getToken()
                if (!semicolonToken!!.match(TokenType.SEMICOLON)) {
                    throw RuntimeException("P: после команды должна следовать \";\"")
                }
            }
            command = parseCommand()
            result.setRight(command)
        } while (!command.getFirstChildren().getValue()!!.match(TokenType.EMPTY))
        result.setRight(Node(TokenType.EMPTY))
        return result
    }

    private fun parseCommand(): Node {
        var result = Node(TokenType.COMMAND)

        val whatEver = lexer.peekToken()
        when (whatEver?.getTokenType()) {
            TokenType.BRACE_CLOSE -> {
            }
            TokenType.INT, TokenType.CHAR -> {
                lexer.getToken()
                val nameToken = lexer.getToken()

                if (nameToken!!.match(TokenType.NAME)) {
                    val typeNode = Node(TokenType.TYPE)
                    typeNode.setLeft(Node(whatEver))
                    result.setLeft(typeNode)
                    result.setRight(Node(nameToken))

                    val openBracetToken = lexer.peekToken()
                    when (openBracetToken?.getTokenType()) {
                        TokenType.BRACET_OPEN -> result.setRight(parseArray())
                        TokenType.ASSIGNMENT -> {
                            var assigment = Node(openBracetToken)
                            lexer.getToken()
                            val strstr = lexer.peekToken()
                            if (strstr!!.match(TokenType.STRSTR)) {
                                assigment = Node(lexer.peekToken()!!)
                                parseStrStr(assigment)
                            } else {
                                assigment.setLeft(parseExpr())
                            }
                            result.setRight(assigment)

                        }
                        else -> {
                        }
                    }
                } else {
                    throw RuntimeException("P: отсутсвует имя у переменной")
                }
            }
            TokenType.NAME -> {
                lexer.getToken()
                val assignToken = lexer.getToken()

                when (assignToken?.getTokenType()) {
                    TokenType.ASSIGNMENT -> {
                        result.setLeft(Node(whatEver))
                        var assigment = Node(assignToken)
                        if (lexer.peekToken()!!.match(TokenType.STRSTR)) {
                            assigment = Node(lexer.peekToken()!!)
                            parseStrStr(assigment)
                        } else {
                            assigment.setLeft(parseExpr())
                        }
                        result.setRight(assigment)
                    }
                    TokenType.BRACET_OPEN -> {
                        result.setLeft(Node(whatEver))
                        result.setRight(parseArrayAssigment())
                    }
                    TokenType.BRACKET_OPEN -> {
                        result = Node(TokenType.CALL_FUNCTION)
                        result.setLeft(Node(whatEver))
                        result.setRight(parseArgList())
                        val nextToken = lexer.getToken()
                        if (!nextToken!!.match(TokenType.BRACKET_CLOSE)) {
                            throw RuntimeException("P: отсвутствует закрывающая скобка")
                        }
                    }
                    else -> throw RuntimeException("P: отсутвует знак = после имени переменной")
                }
            }
            TokenType.RETURN -> {
                lexer.getToken()
                result.setLeft(Node(whatEver))
                result.setRight(parseExpr())
            }
            TokenType.SCANF, TokenType.PRINTF -> {
                lexer.getToken()
                val openBracketToken = lexer.getToken()
                if (!openBracketToken!!.match(TokenType.BRACKET_OPEN)) {
                    throw RuntimeException("P: ожидалась открывающаяся скобка после printf")
                }

                result.setLeft(Node(whatEver))

                result.setRight(parseInOut(whatEver))

                val closeBracketToken = lexer.getToken()
                if (!closeBracketToken!!.match(TokenType.BRACKET_CLOSE)) {
                    throw RuntimeException("P: ожидалась закрывающаяся скобка после printf")
                }
            }
            TokenType.IF -> {
                lexer.getToken()
                val openBracketIfToken = lexer.getToken()
                if (!openBracketIfToken!!.match(TokenType.BRACKET_OPEN)) {
                    throw RuntimeException("P: ожидалась открывающаяся скобка после if")
                }

                result.setLeft(Node(whatEver))
                result.setRight(parseCondition())

                val closeBracketIfToken = lexer.getToken()
                if (!closeBracketIfToken!!.match(TokenType.BRACKET_CLOSE)) {
                    throw RuntimeException("P: ожидалась закрывающаяся скобка после условия")
                }

                val openBracketThenToken = lexer.getToken()
                if (!openBracketThenToken!!.match(TokenType.BRACE_OPEN)) {
                    throw RuntimeException("P: ожидалась открывающаяся фигурная скобка ветви if")
                }

                // ветвь then
                result.setRight(parseBody("then"))
                val closeBracketThenToken = lexer.getToken()
                if (!closeBracketThenToken!!.match(TokenType.BRACE_CLOSE)) {
                    throw RuntimeException("P: ожидалась закрывающаяся фигурная скобка ветви if")
                }

                // проверка на else
                val elseToken = lexer.peekToken()
                if (elseToken!!.match(TokenType.ELSE)) {
                    // есть else ветвь
                    lexer.getToken()
                    val openBraceElseToken = lexer.getToken()
                    if (!openBraceElseToken!!.match(TokenType.BRACE_OPEN)) {
                        throw RuntimeException("P: ожидалась открывающаяся фигурная скобка после else")
                    }
                    result.setRight(parseBody("else"))
                    val closeBraceElseToken = lexer.getToken()
                    if (!closeBraceElseToken!!.match(TokenType.BRACE_CLOSE)) {
                        throw RuntimeException("P: ожидалась закрывающаяся фигурная скобка ветви else")
                    }
                }
            }
            TokenType.WHILE -> {
                lexer.getToken()
                val openBracketWhileToken = lexer.getToken()
                if (!openBracketWhileToken!!.match(TokenType.BRACKET_OPEN)) {
                    throw RuntimeException("P: ожидалась открывающаяся скобка после while")
                }

                result.setLeft(Node(whatEver))
                result.setRight(parseCondition())

                val closeBracketWhileToken = lexer.getToken()
                if (!closeBracketWhileToken!!.match(TokenType.BRACKET_CLOSE)) {
                    throw RuntimeException("P: ожидалась закрывающаяся скобка после условия")
                }

                val openBraceWhileToken = lexer.getToken()
                if (!openBraceWhileToken!!.match(TokenType.BRACE_OPEN)) {
                    throw RuntimeException("P: ожидалась открывающаяся фигурная скобка тела цикла")
                }

                result.setRight(parseBody("def"))

                val closeBraceWhileToken = lexer.getToken()
                if (!closeBraceWhileToken!!.match(TokenType.BRACE_CLOSE)) {
                    throw RuntimeException("P: ожидалась закрывающаяся фигурная скобка тела цикла")
                }
            }
            else -> throw RuntimeException("P: неизвестная команда или не закрыто тело функции")
        }
        result.setRight(Node(TokenType.EMPTY))
        return result
    }

    private fun parseStrStr(assigment: Node) {
        lexer.getToken()
        val openBracketToken = lexer.getToken()
        if (!openBracketToken!!.match(TokenType.BRACKET_OPEN)) {
            throw RuntimeException("ошибка ( скобки")
        }
        val name1Token = lexer.getToken()

        if (!name1Token!!.match(TokenType.NAME)) {
            throw RuntimeException("ошибка первого аргумента")
        }

        assigment.setRight(Node(name1Token))

        val commaToken = lexer.getToken()

        if (!commaToken!!.match(TokenType.COMMA)) {
            throw RuntimeException("ошибка запятая между аргументами аргумента")
        }

        val name2Token = lexer.getToken()

        if (!name2Token!!.match(TokenType.NAME)) {
            throw RuntimeException("ошибка второго аргумента")
        }

        assigment.setRight(Node(name2Token))

        val closeBracketToken = lexer.getToken()
        if (!closeBracketToken!!.match(TokenType.BRACKET_CLOSE)) {
            throw RuntimeException("ошибка ) скобки")
        }
    }

    private fun parseInOut(out: Token<*>): Node {

        val result: Node = when (out.getTokenType()) {
            TokenType.PRINTF -> Node(TokenType.PRINTF_BODY)
            TokenType.SCANF -> Node(TokenType.SCANF_BODY)
            else -> throw IllegalStateException("Unexpected value: " + out.getTokenType())
        }

        val literalToken = lexer.getToken()
        if (literalToken!!.match(TokenType.LITERAL)) {
            result.setRight(Node(literalToken))
        }

        // TODO: возможно научить выводить массив по индексам

        val commaToken = lexer.peekToken()
        if (commaToken!!.match(TokenType.BRACKET_CLOSE)) {
            return result
        } else if (commaToken.match(TokenType.COMMA)) {
            lexer.getToken()
            while (!lexer.peekToken()!!.match(TokenType.BRACKET_CLOSE)) {
                val tokenName = lexer.getToken()
                if (tokenName!!.match(TokenType.NAME)) {
                    result.setRight(Node(tokenName))
                }
            }
        }
        return result
    }

    private fun parseArrayAssigment(): Node {
        val result = Node(TokenType.ARRAYASSIGMENT)

        val numberToken = lexer.getToken()

        when (numberToken?.getTokenType()) {
            TokenType.NUMBER, TokenType.NAME -> result.setRight(Node(numberToken))
            else -> throw RuntimeException("P: не указан индекс массива")
        }

        val bracketClose = lexer.getToken()
        if (!bracketClose!!.match(TokenType.BRACET_CLOSE)) {
            throw RuntimeException("P: не закрыта фигурная скобка индекса массива")
        }

        val assignToken = lexer.getToken()
        if (assignToken!!.match(TokenType.ASSIGNMENT)) {
            val assigment = Node(assignToken)
            when (lexer.peekToken()?.getTokenType()) {
                TokenType.NUMBER, TokenType.NAME -> assigment.setRight(parseExpr())
                else -> throw RuntimeException("P: неизвестный тип")
            }
            result.setRight(assigment)
        } else {
            throw RuntimeException("P: отсутсвует знак = у массива")
        }
        return result
    }

    private fun parseArray(): Node {
        val result = Node(TokenType.ARRAY)

        val tet = lexer.getToken()
        val numberToken = lexer.getToken()

        when (numberToken?.getTokenType()) {
            TokenType.NUMBER, TokenType.NAME -> result.setLeft(Node(numberToken))
            else -> throw RuntimeException("P: не указан индекс массива")
        }
        val closeBracetToken = lexer.getToken()
        if (closeBracetToken!!.match(TokenType.BRACET_CLOSE)) {
            val assignToken = lexer.peekToken()
            when (assignToken?.getTokenType()) {
                TokenType.ASSIGNMENT -> result.setRight(parseArrayBody())
                TokenType.BRACKET_CLOSE, TokenType.SEMICOLON, TokenType.SIGN -> return result
                else -> throw RuntimeException("P: Ошибка массива")
            }
        }
        return result
    }

    private fun parseArrayBody(): Node {
        val result = Node(TokenType.ARRAY_BODY)

        lexer.getToken()
        val openBraceToken = lexer.getToken()

        when (openBraceToken?.getTokenType()) {
            TokenType.BRACE_OPEN -> while (!lexer.peekToken()!!.match(TokenType.BRACE_CLOSE)) {
                val numberToken = lexer.getToken()
                if (numberToken!!.match(TokenType.NUMBER)) {
                    result.setRight(Node(numberToken))
                }
                val commaToken = lexer.getToken()
                if (commaToken!!.match(TokenType.BRACE_CLOSE)) {
                    break
                }
            }
            TokenType.LITERAL -> result.setRight(Node(openBraceToken))
            else -> throw RuntimeException("P: Ошибка тела массива")
        }
        return result
    }

    private fun parseCondition(): Node {
        val result = Node(TokenType.CONDITION)
        result.setLeft(parseExpr())
        val signToken = lexer.getToken()
        if (!signToken!!.match(TokenType.SIGN)) {
            throw RuntimeException("P: ожидался знак условия")
        }
        result.setRight(Node(signToken))
        result.setRight(parseExpr())
        result.setRight(Node(TokenType.EMPTY))
        return result
    }

    private fun parseArgList(): Node {
        val closeBracketToken = lexer.peekToken()
        val result = Node(TokenType.ARG_LIST, closeBracketToken!!.row, closeBracketToken.col - 1)
        if (closeBracketToken.match(TokenType.BRACKET_CLOSE)) {
            result.setLeft(Node(TokenType.EMPTY))
            return result
        }
        var commaToken: Token<*>?
        do {
            result.setRight(parseExpr())
            commaToken = lexer.peekToken()
        } while (commaToken!!.match(TokenType.COMMA) && commaToken === lexer.getToken())
        if (!commaToken.match(TokenType.BRACKET_CLOSE)) {
            throw RuntimeException("P: отсутствует )")
        }
        return result
    }

    private fun parseType(): Node {
        val result: Node
        val typeToken = lexer.getToken()
        when (typeToken?.getTokenType()) {
            TokenType.INT, TokenType.VOID, TokenType.CHAR -> {
                val specificType = Node(typeToken)
                result = Node(TokenType.TYPE)
                result.setLeft(specificType)
            }
            else -> throw RuntimeException("P: неизвестный тип")
        }
        return result
    }

    private fun parseExpr(): Node {
        var result: Node = parseTerm()
        var curToken = lexer.peekToken()
        while (curToken!!.match(TokenType.PLUS) ||
                curToken.match(TokenType.MINUS)) {
            lexer.getToken()
            val sign = Node(curToken)
            sign.setLeft(result)
            sign.setRight(parseTerm())
            result = sign
            curToken = lexer.peekToken()
        }
        return result
    }

    private fun parseTerm(): Node {
        var result: Node = parseFactor()
        var curToken = lexer.peekToken()
        while (curToken!!.match(TokenType.MULTIPLICATION) ||
                curToken.match(TokenType.DIVISION)) {
            lexer.getToken()
            val sign = Node(curToken)
            sign.setLeft(result)
            sign.setRight(parseFactor())
            result = sign
            curToken = lexer.peekToken()
        }
        return result
    }

    private fun parseFactor(): Node {
        var result: Node = parsePower()
        val curToken = lexer.peekToken()
        if (curToken!!.match(TokenType.EXPONENTIATION)) {
            lexer.getToken()
            val exp = Node(curToken)
            exp.setLeft(result)
            exp.setRight(parseFactor())
            result = exp
        }
        return result
    }

    private fun parsePower(): Node {
        val curToken = lexer.peekToken()
        if (curToken!!.match(TokenType.MINUS)) {
            lexer.getToken()
            val minus = Node(curToken)
            minus.setLeft(parseAtom())
            return minus
        }
        return parseAtom()
    }

    private fun parseAtom(): Node {
        val result: Node
        var token = lexer.getToken()
        when (token?.getTokenType()) {
            TokenType.BRACKET_OPEN -> {
                result = parseExpr()
                token = lexer.getToken()
                return if (token!!.match(TokenType.BRACKET_CLOSE)) {
                    result
                } else {
                    throw RuntimeException("P: отсутсвует закрывающая скобка")
                }
            }
            TokenType.LITERAL, TokenType.CHAR, TokenType.NUMBER -> result = Node(token)
            TokenType.NAME -> {
                var nextToken = lexer.peekToken()
                when (nextToken?.getTokenType()) {
                    TokenType.BRACKET_OPEN -> {
                        lexer.getToken()
                        result = Node(TokenType.CALL_FUNCTION)
                        result.setLeft(Node(token))
                        result.setRight(parseArgList())
                        nextToken = lexer.getToken()
                        if (!nextToken!!.match(TokenType.BRACKET_CLOSE)) {
                            throw RuntimeException("P: отсвутствует закрывающая скобка")
                        }
                    }
                    TokenType.BRACET_OPEN -> {
                        result = Node(token)
                        result.setLeft(parseArray())
                    }
                    else -> result = Node(token)
                }
            }
            else -> throw RuntimeException("P: неизвестный тип")
        }
        return result
    }

    private fun checkExceptionSemicolon(command: Node): Boolean {
        return !(command.getFirstChildren().getTokenType() === TokenType.IF ||
                command.getFirstChildren().getTokenType() === TokenType.WHILE)
    }
}