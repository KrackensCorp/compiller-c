package parser

import lexer.Token
import lexer.TokenType
import java.io.FileNotFoundException
import java.io.PrintWriter
import java.util.*

class Node constructor(typeValue: Any) : Cloneable {

    private var value: Token<*>? = null
    private var listChild: MutableList<Node> = ArrayList()
    private lateinit var parent: Node

    init {
        if (typeValue is TokenType) {
            value = Token<Any>(typeValue)
            listChild = ArrayList()
        }
        if (typeValue is Token<*>) {
            value = typeValue
            listChild = ArrayList()
        }
    }

    constructor(typeValue: TokenType, row: Int, col: Int) : this(typeValue) {
        value = Token<Any>(typeValue, row, col)
        listChild = ArrayList()
    }

    @Throws(CloneNotSupportedException::class)
    public override fun clone(): Node {
        return super.clone() as Node
    }

    fun getValue(): Token<*>? {
        return value
    }

    fun getTokenType(): TokenType {
        return value!!.getTokenType()
    }

    fun getTokenValue(): Any? {
        return value?.getTokenValue()
    }

    fun setValue(value: Token<*>) {
        this.value = value
    }

    fun setLeft(leftChild: Node) {
        listChild.add(0, leftChild)
    }

    fun changeNode(typeValue: TokenType) {
        value = Token<Any>(typeValue)
        listChild = ArrayList()
    }

    fun setRight(rightChild: Node) {
        listChild.add(rightChild)
    }

    fun getFirstChildren(): Node {
        return listChild[0]
    }

    fun getParent(): Node {
        return parent
    }

    fun setParent(parent: Node) {
        this.parent = parent
    }

    fun getListChild(): MutableList<Node> {
        return listChild
    }

    fun match(type: TokenType): Boolean {
        return value!!.match(type)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other == null) {
            return false
        }
        if (javaClass != other.javaClass) {
            return false
        }
        val oother = other as Node
        if (listChild != oother.listChild) {
            return false
        }
        return if (value == null) {
            oother.value == null
        } else value == oother.value
    }

    fun writeGraph(path: String) {
        try {
            val writer = PrintWriter(path)
            writer.println("digraph parseTree {")
            writer.println("\tordering=out;")
            writeGraph(writer, VertexNumber())
            writer.println("}")
            writer.close()
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
    }

    private fun writeGraph(writer: PrintWriter, vertex: VertexNumber) {
        if (listChild.size == 0) {
            writer.println(String.format("\ta_%s [label=\"%s\"; style=filled; fillcolor=green;];", vertex.value, getTokenType()))
        } else {
            writer.println(String.format("\ta_%s [label=\"%s\"];", vertex.value, value?.getTokenType()))
        }
        val successors: MutableList<Int> = ArrayList()
        val curVertex = vertex.value
        for (t in listChild) {
            vertex.inc()
            successors.add(vertex.value)
            t.writeGraph(writer, vertex)
        }
        for (i in successors) {
            writer.println(String.format("\ta_%s -> a_%s", curVertex, i))
        }
    }

    private class VertexNumber {
        var value = 0
        fun inc() {
            ++value
        }
    }

    override fun toString(): String {
        return "Node{" +
                "parent=" + parent +
                ", value=" + value +
                ", listChild=" + listChild +
                '}'
    }

    override fun hashCode(): Int {
        var result = value.hashCode()
        result = 31 * result + listChild.hashCode()
        result = 31 * result + (parent.hashCode())
        return result
    }
}
