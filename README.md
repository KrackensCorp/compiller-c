# С compiler
Написанный на яп Kotlin компилятор яп Си в бинарный файл.  
Linux  
GCC 9.3+  
JDK 1.8  
## Загрузка
```
git clone https://gitlab.com/KrackensCorp/compiller-c.git
```

## Запуск в IntelliJIDEA  
Program arguments: ```<args> <filepath>```  
Поддерживаемые аргументы:
```
--dump-token <filepath>  - для разложения программы на токены и их вывода в терминал
--dump-ast <filepath>  - для построения обычного (./tmp/ast.dot) и аннотированного (./tmp/semaAst.dot) ast дерева кода программы
--dump-asm <filepath>  - для вывода в ".s" файл и терминал ассемблерного кода программы
<filepath>  - для обычной компиляции в бинарный файл  
```
Пример: ```--dump-asm ./examples/min_max_array.c```  
Готовые тестовые файлы для компиляции находятся в директории ./examples   
Для обычной компиляции в бинарник введите только путь до ".c" исходника.  


